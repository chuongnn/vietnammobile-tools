package com.cimb.vietnammobile.tools.util;

import pl.touk.throwing.exception.WrappedException;

public class ExceptionUtils {
    public static Throwable causeAsThrowableOf(Throwable ex) {
        return ex instanceof WrappedException
                ? ex.getCause()
                : ex;
    }

    public static Exception causeAsExceptionOf(Throwable ex) {
        Throwable cause = ex instanceof WrappedException
                ? ex.getCause()
                : ex;
        return cause instanceof Exception
                ? (Exception) cause
                : new Exception(cause);
    }
}
