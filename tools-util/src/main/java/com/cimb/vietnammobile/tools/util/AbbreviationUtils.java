package com.cimb.vietnammobile.tools.util;

import org.apache.commons.lang3.StringUtils;

import javax.crypto.SecretKey;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.List;
import java.util.Optional;

public class AbbreviationUtils {
    private static final String DELIMITER = ",";
    private static final int ABBREVIATE_LENGTH_RSA_KEY = 8;
    private static final int ABBREVIATE_LENGTH_SESSION_KEY = 8;
    private static final int ABBREVIATE_LENGTH_DATA = 8;
    private static final int ABBREVIATE_LENGTH_EXCEPTION = 128;
    private static final String P_ABBREVIATION_MIDDLE = "xxx";
    private static final int P_ABBREVIATION_LENGTH = 3;

    public static String of(PublicKey publicKey) {
        return StringUtils.abbreviate(
                Optional.ofNullable(publicKey)
                        .map(PublicKey::getEncoded)
                        .map(Object::toString)
                        .orElse(null),
                ABBREVIATE_LENGTH_RSA_KEY);
    }

    public static String of(PrivateKey privateKey) {
        return StringUtils.abbreviate(
                Optional.ofNullable(privateKey)
                        .map(PrivateKey::getEncoded)
                        .map(Object::toString)
                        .orElse(null),
                ABBREVIATE_LENGTH_RSA_KEY);
    }

    public static String of(SecretKey secretKey) {
        return StringUtils.abbreviate(
                Optional.ofNullable(secretKey)
                        .map(SecretKey::getEncoded)
                        .map(Object::toString)
                        .orElse(null),
                ABBREVIATE_LENGTH_SESSION_KEY);
    }

    public static String of(byte[] data) {
        return StringUtils.abbreviate(
                Optional.ofNullable(data)
                        .map(Object::toString)
                        .orElse(null),
                ABBREVIATE_LENGTH_DATA);
    }

    public static String of(String data) {
        return StringUtils.abbreviate(data, ABBREVIATE_LENGTH_DATA);
    }

    public static String of(Throwable ex) {
        return StringUtils.abbreviate(ex.getMessage(), ABBREVIATE_LENGTH_EXCEPTION);
    }

    public static String of(List<String> lines) {
        return StringUtils.abbreviate(StringUtils.join(lines, DELIMITER), ABBREVIATE_LENGTH_DATA);
    }

    public static String ofPassword(String password) {
        return StringUtils.abbreviateMiddle(password, P_ABBREVIATION_MIDDLE, P_ABBREVIATION_LENGTH);
    }

    public static String ofRSAKeyObject(Object data) {
        return StringUtils.abbreviate(String.valueOf(data), ABBREVIATE_LENGTH_RSA_KEY);
    }
}
