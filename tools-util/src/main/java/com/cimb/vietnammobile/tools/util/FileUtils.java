package com.cimb.vietnammobile.tools.util;

import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import static com.cimb.vietnammobile.tools.util.ExceptionUtils.causeAsExceptionOf;
import static pl.touk.throwing.ThrowingFunction.unchecked;

public class FileUtils {
    public static Path pathOf(String pathDescription) throws Exception {
        try {
            return Optional.ofNullable(pathDescription)
                    .map(unchecked(ResourceUtils::getURL))
                    .map(unchecked(URL::toURI))
                    .map(Paths::get)
                    .orElse(null);
        } catch (Throwable ex) {
            throw causeAsExceptionOf(ex);
        }
    }

    public static Path existingReadableFilePathOf(String pathDescription) throws Exception {
        try {
            return Optional.ofNullable(pathDescription)
                    .map(unchecked(FileUtils::pathOf))
                    .filter(Files::exists)
                    .filter(Files::isRegularFile)
                    .filter(Files::isReadable)
                    .orElse(null);
        } catch (Throwable ex) {
            throw causeAsExceptionOf(ex);
        }
    }

    public static File existingReadableFileOf(String pathDescription) throws Exception {
        try {
            return Optional.ofNullable(pathDescription)
                    .map(unchecked(FileUtils::existingReadableFilePathOf))
                    .map(unchecked(Path::toFile))
                    .orElse(null);
        } catch (Throwable ex) {
            throw causeAsExceptionOf(ex);
        }
    }

    public static InputStream existingReadableInputStreamOf(String pathDescription) throws Exception {
        try {
            return Optional.ofNullable(pathDescription)
                    .map(unchecked(FileUtils::existingReadableFilePathOf))
                    .map(unchecked(Files::newInputStream))
                    .orElse(null);
        } catch (Throwable ex) {
            throw causeAsExceptionOf(ex);
        }
    }

    public static String existingReadableFileNameDescriptionOf(String pathDescription) throws Exception {
        try {
            return Optional.ofNullable(pathDescription)
                    .map(unchecked(FileUtils::existingReadableFilePathOf))
                    .map(Path::getFileName)
                    .map(Path::toString)
                    .orElse(null);
        } catch (Throwable ex) {
            throw causeAsExceptionOf(ex);
        }
    }
}
