package com.cimb.vietnammobile.tool.promotion.it;

import com.cimb.vietnammobile.tool.promotion.it.mocked.api.model.request.TransitStateRestfulRequest;
import com.cimb.vietnammobile.tool.promotion.it.mocked.api.statemachine.State;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContextBuilder;
import org.junit.After;
import org.junit.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.ResourceUtils;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import java.io.InputStream;
import java.net.URL;
import java.security.KeyStore;

public class BaseIT {
    private static final Logger log = LoggerFactory.getLogger(BaseIT.class);
    private static final String LOG_TEMPLATE_STATE_TRANSITING_RESULT = "Transit to state=%s, transitingResult=%s";

    private RestTemplate stateMachineRestTemplate = stateMachineRestTemplate(new RestTemplateBuilder());

    @Before
    public void setUp() {
        transitMockedAPIStateTo(State.NORMAL);
    }

    @After
    public void tearDown() {
        transitMockedAPIStateTo(State.NORMAL);
    }

    protected void transitMockedAPIStateTo(State state) {
        ResponseEntity responseEntity = stateMachineRestTemplate.postForEntity(
                Constants.MOCKED_API_URL_STATE_MACHINE,
                TransitStateRestfulRequest
                        .newBuilder()
                        .withState(state)
                        .build(),
                Object.class);
        log.info(String.format(LOG_TEMPLATE_STATE_TRANSITING_RESULT, state, responseEntity.toString()));
    }

    private RestTemplate stateMachineRestTemplate(RestTemplateBuilder restTemplateBuilder) {
        return restTemplateBuilder
                .additionalInterceptors(
                        interceptor()
                ).requestFactory(
                        () -> requestFactory(
                                httpClient(
                                        connectionManager(
                                                registry(
                                                        sslSocketFactory(
                                                                sslContext(
                                                                        trustStore(
                                                                        )
                                                                ),
                                                                hostnameVerifier()
                                                        )
                                                )
                                        )
                                )
                        )
                ).build();
    }

    private ClientHttpRequestInterceptor interceptor() {
        return (request, body, execution) -> {
            request.getHeaders().set(Constants.CONTENT_TYPE_KEYWORD, MediaType.APPLICATION_JSON_VALUE);
            request.getHeaders().set(Constants.ACCEPT_KEYWORD, MediaType.APPLICATION_JSON_VALUE);
            return execution.execute(request, body);
        };
    }

    private HttpComponentsClientHttpRequestFactory requestFactory(
            CloseableHttpClient httpClient
    ) {
        HttpComponentsClientHttpRequestFactory factory =
                new HttpComponentsClientHttpRequestFactory(httpClient);
        factory.setConnectionRequestTimeout(Constants.MOCKED_API_CONNECTION_REQUEST_TIMEOUT);
        factory.setConnectTimeout(Constants.MOCKED_API_CONNECT_TIMEOUT);
        factory.setReadTimeout(Constants.MOCKED_API_READ_TIMEOUT);
        return factory;
    }

    private CloseableHttpClient httpClient(PoolingHttpClientConnectionManager connectionManager) {
        return HttpClients
                .custom()
                .setConnectionManager(connectionManager)
                .build();
    }

    private PoolingHttpClientConnectionManager connectionManager(
            Registry<ConnectionSocketFactory> registry) {
        PoolingHttpClientConnectionManager manager = new PoolingHttpClientConnectionManager(registry);
        manager.setMaxTotal(Constants.MOCKED_API_MAX_TOTAL);
        manager.setDefaultMaxPerRoute(Constants.DEFAULT_MAX_PER_ROUTE);
        return manager;
    }

    private Registry<ConnectionSocketFactory> registry(SSLConnectionSocketFactory sslSocketFactory) {
        return RegistryBuilder.<ConnectionSocketFactory>create()
                .register(Constants.HTTPS_KEYWORD, sslSocketFactory)
                .build();
    }

    private SSLConnectionSocketFactory sslSocketFactory(
            SSLContext sslContext,
            HostnameVerifier hostnameVerifier) {
        return new SSLConnectionSocketFactory(sslContext, hostnameVerifier);
    }

    private SSLContext sslContext(KeyStore trustStore) {
        try {
            return new SSLContextBuilder()
                    .loadTrustMaterial(trustStore, new TrustSelfSignedStrategy())
                    .build();
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }

    private HostnameVerifier hostnameVerifier() {
        return new NoopHostnameVerifier();
    }

    private KeyStore trustStore() {
        try {
            final KeyStore store = KeyStore.getInstance(Constants.MOCKED_API_TRUST_STORE_TYPE);
            URL url = ResourceUtils.getURL(Constants.MOCKED_API_TRUST_STORE_FILE_PATH);
            InputStream inputStream = url.openStream();
            store.load(inputStream, Constants.MOCKED_API_TRUST_STORE_PASSWORD.toCharArray());
            System.out.println("hello");
            return store;
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }
}
