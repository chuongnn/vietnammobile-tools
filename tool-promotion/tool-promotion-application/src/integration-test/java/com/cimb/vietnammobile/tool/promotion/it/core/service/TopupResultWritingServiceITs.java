package com.cimb.vietnammobile.tool.promotion.it.core.service;

import com.cimb.vietnammobile.tool.promotion.core.configuration.CoreConfiguration;
import com.cimb.vietnammobile.tool.promotion.core.model.TopupResult;
import com.cimb.vietnammobile.tool.promotion.core.service.TopupResultWritingService;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.OutputFileInterface;
import com.cimb.vietnammobile.tool.promotion.it.BaseIT;
import com.cimb.vietnammobile.tool.promotion.it.Constants;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.file.Files;

@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:application-it.properties")
@SpringBootTest(
        classes = {
                CoreConfiguration.class
        })
public class TopupResultWritingServiceITs extends BaseIT {
    @Autowired
    private TopupResultWritingService topupResultWritingService;
    @Autowired
    private OutputFileInterface outputFileInterface;

    @Test
    public void bootstrap_shouldInitiateTheService_Successfully() {
        Assert.assertNotNull(topupResultWritingService);
    }

    @Test
    public void write_shouldAppendTheTopupResultToOutputFile_AfterProcessValidTopupResult() throws IOException {
        //GIVEN: a valid topupResult
        TopupResult result = topupResult();

        //WHEN: call write with above topupResult
        topupResultWritingService.write(result);

        //THEN: topupResult is appendToFile
        String content = new String(Files.readAllBytes(outputFileInterface.outputFile()));
        Assert.assertNotNull(content);
        Assert.assertTrue(StringUtils.contains(content, topupResult().getPhoneNumber()));
        Assert.assertTrue(StringUtils.contains(content, topupResult().getTransactionId()));
        Assert.assertTrue(StringUtils.contains(content, String.valueOf(topupResult().getResultCode())));
        Assert.assertTrue(StringUtils.contains(content, topupResult().getResultMessage()));
        Assert.assertTrue(StringUtils.contains(content, topupResult().getRemark()));
    }

    private TopupResult topupResult() {
        return TopupResult
                .newBuilder()
                .withPhoneNumber(Constants.SAMPLE_PHONE_NUMBER)
                .withTransactionId(Constants.SAMPLE_TRANSACTION_ID)
                .withResultCode(com.cimb.vietnammobile.tool.promotion.core.configuration.Constants.RESULT_CODE_SUCCESS)
                .withResultMessage(Constants.SAMPLE_RESULT_MESSAGE_SUCCESS)
                .withRemark(Constants.SAMPLE_REMARKS_SUCCESS)
                .build();
    }
}
