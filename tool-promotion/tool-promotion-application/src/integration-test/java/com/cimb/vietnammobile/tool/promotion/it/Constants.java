package com.cimb.vietnammobile.tool.promotion.it;

import org.apache.commons.lang3.StringUtils;

public class Constants {
    public static final String MOCKED_API_URL_STATE_MACHINE = "https://localhost:8080/topup/statemachine";
    public static final String HTTPS_KEYWORD = "https";
    public static final String CONTENT_TYPE_KEYWORD = "Content-Type";
    public static final String ACCEPT_KEYWORD = "Accept";
    public static final int MOCKED_API_CONNECTION_REQUEST_TIMEOUT = 1000;
    public static final int MOCKED_API_CONNECT_TIMEOUT = 1000;
    public static final int MOCKED_API_READ_TIMEOUT = 15000;
    public static final int MOCKED_API_MAX_TOTAL = 200;
    public static final int DEFAULT_MAX_PER_ROUTE = 200;
    public static final String MOCKED_API_TRUST_STORE_TYPE = "JKS";
    public static final String MOCKED_API_TRUST_STORE_FILE_PATH = "classpath:keystore/mockedVietnammobileInterface.truststore.jks";
    public static final String MOCKED_API_TRUST_STORE_PASSWORD = "password";
    public static final String SAMPLE_PHONE_NUMBER = "0987654321";
    public static final String SAMPLE_TRANSACTION_ID = "000001";
    public static final String SAMPLE_RESULT_MESSAGE_SUCCESS = "success";
    public static final String SAMPLE_REMARKS_SUCCESS = StringUtils.EMPTY;
    public static final String SAMPLE_INPUT_FILE = "classpath:SampleInputFile.csv";
    public static final String SAMPLE_OUTPUT_FILE = "classpath:SampleOutputFile.csv";
    public static final String SAMPLE_OUTPUT_FILE_NO_FAILED = "classpath:SampleNoFailedOutputFile.csv";
}
