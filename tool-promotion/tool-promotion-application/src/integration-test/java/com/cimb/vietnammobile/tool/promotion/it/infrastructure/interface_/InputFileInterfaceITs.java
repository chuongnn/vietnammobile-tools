package com.cimb.vietnammobile.tool.promotion.it.infrastructure.interface_;

import com.cimb.vietnammobile.tool.promotion.infrastructure.configuration.InfraStructureConfiguration;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.InputFileInterface;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.properties.InputFileInterfaceProperties;
import com.cimb.vietnammobile.tool.promotion.it.BaseIT;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.nio.file.Path;

@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:application-it.properties")
@SpringBootTest(
        classes = {
                InfraStructureConfiguration.class
        },
        properties = {
                "vietnammobile.interface.input-file.file-name=classpath:SampleInputFile.csv"
        })
public class InputFileInterfaceITs extends BaseIT {
    @Autowired
    private InputFileInterfaceProperties inputFileInterfaceProperties;
    @Autowired
    private InputFileInterface inputFileInterface;

    @Test
    public void bootstrap_shouldInitiateInterface_Successfully() {
        Assert.assertNotNull(inputFileInterface);
    }

    @Test
    public void inputFile_shouldReturnAPathObject_afterProcessingAValidFileName() {
        //GIVEN: a valid fileName
        Assert.assertNotNull(inputFileInterfaceProperties.getFileName());

        //WHEN: call inputFile
        Path inputFile = inputFileInterface.inputFile();

        //THEN: a valid Path object is returned
        Assert.assertNotNull(inputFile);
    }
}
