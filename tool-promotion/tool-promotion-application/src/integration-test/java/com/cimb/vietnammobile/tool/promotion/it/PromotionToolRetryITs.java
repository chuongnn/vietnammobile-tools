package com.cimb.vietnammobile.tool.promotion.it;

import com.cimb.vietnammobile.tool.promotion.PromotionTool;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.InputFileInterface;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.OutputFileInterface;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.nio.file.Files;

@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:application-it.properties")
@SpringBootTest(
        classes = {
                PromotionTool.class
        },
        properties = {
                "vietnammobile.interface.command.command=retry",
                "vietnammobile.interface.input-file.file-name=classpath:SampleOutputFile.csv",
        })
public class PromotionToolRetryITs extends BaseIT {
    @Autowired
    private InputFileInterface inputFileInterface;
    @Autowired
    private OutputFileInterface outputFileInterface;

    @Test
    public void run_shouldExitNormally_afterProcessingAValidInputFile() throws IOException {
        //GIVEN: a valid inputFile
        Assert.assertNotNull(inputFileInterface.inputFile());

        //WHEN: call run
        //THEN: application exit normally
        String outputContent = new String(Files.readAllBytes(outputFileInterface.outputFile()));
        Assert.assertNotNull(outputContent);
    }
}
