package com.cimb.vietnammobile.tool.promotion.it.infrastructure.interface_;

import com.cimb.vietnammobile.tool.promotion.infrastructure.configuration.InfraStructureConfiguration;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.OutputFileInterface;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.properties.OutputFileInterfaceProperties;
import com.cimb.vietnammobile.tool.promotion.it.BaseIT;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.nio.file.Path;

@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:application-it.properties")
@SpringBootTest(
        classes = {
                InfraStructureConfiguration.class
        },
        properties = {
                "vietnammobile.interface.output-file.folder-name=",
                "vietnammobile.interface.output-file.file-name="
        })
public class OutputFileInterfaceITs extends BaseIT {
    @Autowired
    private OutputFileInterfaceProperties outputFileInterfaceProperties;
    @Autowired
    private OutputFileInterface outputFileInterface;

    @Test
    public void bootstrap_shouldInitiateService_Successfully() {
        Assert.assertNotNull(outputFileInterface);
    }

    @Test
    public void outputFile_shouldReturnAValidOutputFilePath_afterProcessingBlankOutputFolderAndOutputFile() {
        //GIVEN: blank outputFolder and outputFile
        Assert.assertTrue(StringUtils.isBlank(outputFileInterfaceProperties.getFolderName()));
        Assert.assertTrue(StringUtils.isBlank(outputFileInterfaceProperties.getFileName()));

        //WHEN: call outputFile
        Path outputFile = outputFileInterface.outputFile();

        //THEN: a valid path is return
        Assert.assertNotNull(outputFile);
    }
}
