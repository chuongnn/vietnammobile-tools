package com.cimb.vietnammobile.tool.promotion.it.core.service;

import com.cimb.vietnammobile.tool.promotion.core.configuration.CoreConfiguration;
import com.cimb.vietnammobile.tool.promotion.core.service.TransactionIdGeneratingService;
import com.cimb.vietnammobile.tool.promotion.core.service.properties.TransactionIdProperties;
import com.cimb.vietnammobile.tool.promotion.it.BaseIT;
import com.cimb.vietnammobile.tool.promotion.it.Constants;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:application-it.properties")
@SpringBootTest(
        classes = {
                CoreConfiguration.class
        })
public class TransactionIdGeneratingServiceITs extends BaseIT {
    @Autowired
    private TransactionIdGeneratingService transactionIdGeneratingService;
    @Autowired
    private TransactionIdProperties transactionIdProperties;

    @Test
    public void bootstrap_shouldInitiateService_Successfully() {
        Assert.assertNotNull(transactionIdGeneratingService);
    }

    @Test
    public void transactionIdFor_shouldReturnAValidTransactionId_afterProcessingAValidPhoneNumber() {
        //GIVEN: a valid phoneNumber
        String phoneNumber = phoneNumber();

        //WHEN: call transactionIdFor with above phoneNumber
        String transactionId = transactionIdGeneratingService.transactionIdFor(phoneNumber);

        //THEN: a valid transactionId is returned
        Assert.assertNotNull(transactionId);
        Assert.assertTrue(StringUtils.contains(transactionId, transactionIdProperties.getPrefix()));
    }

    private String phoneNumber() {
        return Constants.SAMPLE_PHONE_NUMBER;
    }
}
