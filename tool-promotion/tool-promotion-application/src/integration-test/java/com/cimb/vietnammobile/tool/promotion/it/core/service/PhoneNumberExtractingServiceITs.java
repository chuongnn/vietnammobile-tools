package com.cimb.vietnammobile.tool.promotion.it.core.service;

import com.cimb.vietnammobile.tool.promotion.core.configuration.CoreConfiguration;
import com.cimb.vietnammobile.tool.promotion.core.service.PhoneNumberExtractingService;
import com.cimb.vietnammobile.tool.promotion.it.BaseIT;
import com.cimb.vietnammobile.tool.promotion.it.Constants;
import com.cimb.vietnammobile.tools.util.FileUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;

import java.nio.file.Path;
import java.util.List;

@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:application-it.properties")
@SpringBootTest(
        classes = {
                CoreConfiguration.class
        })
public class PhoneNumberExtractingServiceITs extends BaseIT {
    @Autowired
    private PhoneNumberExtractingService phoneNumberExtractingService;

    @Test
    public void bootstrap_shouldInitiateService_Successfully() {
        Assert.assertNotNull(phoneNumberExtractingService);
    }

    @Test
    public void extract_shouldReturnAListOfPhoneNumber_afterProcessingAValidInputFile() throws Exception {
        //GIVEN: a valid inputFile
        Path inputFile = FileUtils.existingReadableFilePathOf(Constants.SAMPLE_INPUT_FILE);

        //WHEN: call extract with above inputFile
        List<String> phoneNumbers = phoneNumberExtractingService.extract(inputFile);

        //THEN: a list of phone numbers is return
        Assert.assertNotNull(phoneNumbers);
        Assert.assertFalse(CollectionUtils.isEmpty(phoneNumbers));
    }
}
