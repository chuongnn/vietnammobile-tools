package com.cimb.vietnammobile.tool.promotion.it.core.service;

import com.cimb.vietnammobile.tool.promotion.core.configuration.CoreConfiguration;
import com.cimb.vietnammobile.tool.promotion.core.model.TopupRequest;
import com.cimb.vietnammobile.tool.promotion.core.model.TopupResult;
import com.cimb.vietnammobile.tool.promotion.core.service.TopupService;
import com.cimb.vietnammobile.tool.promotion.it.BaseIT;
import com.cimb.vietnammobile.tool.promotion.it.Constants;
import com.cimb.vietnammobile.tool.promotion.it.mocked.api.statemachine.State;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:application-it.properties")
@SpringBootTest(
        classes = {
                CoreConfiguration.class
        })
public class TopupServiceITs extends BaseIT {
    @Autowired
    private TopupService topupService;

    @Test
    public void bootstrap_shouldInitiateService_Successfully() {
        Assert.assertNotNull(topupService);
    }

    @Test
    public void topup_shouldProduceResultCodeOfSuccess_whenMockedAPIIsNormal() {
        //GIVEN: mockedAPIs is NORMAL
        transitMockedAPIStateTo(State.NORMAL);

        //WHEN: call topup
        TopupResult result = topupService.topup(topupRequest());

        //THEN: a topupResult is returned
        Assert.assertNotNull(result);
        Assert.assertEquals(
                result.getResultCode(),
                com.cimb.vietnammobile.tool.promotion.core.configuration.Constants.RESULT_CODE_SUCCESS);
    }

    @Test
    public void topup_shouldProduceResultCodeOfCommunicationError_whenLoginReturnsUnknownHttpStatus() {
        //GIVEN: mockedAPIs is LOGIN_RETURN_UNKNOWN_HTTP_STATUS
        transitMockedAPIStateTo(State.LOGIN_RETURN_UNKNOWN_HTTP_STATUS);

        //WHEN: call topup
        TopupResult result = topupService.topup(topupRequest());

        //THEN: a topupResult is returned
        Assert.assertNotNull(result);
        Assert.assertEquals(
                result.getResultCode(),
                com.cimb.vietnammobile.tool.promotion.core.configuration.Constants.RESULT_CODE_COMMUNICATION_ERROR);
    }

    @Test
    public void topup_shouldProduceResultCodeOfCommunicationError_whenLoginReturnsEmptyInternalServerError() {
        //GIVEN: mockedAPIs is LOGIN_RETURNED_EMPTY_INTERNAL_SERVER_ERROR
        transitMockedAPIStateTo(State.LOGIN_RETURNED_EMPTY_INTERNAL_SERVER_ERROR);

        //WHEN: call topup
        TopupResult result = topupService.topup(topupRequest());

        //THEN: a topupResult is returned
        Assert.assertNotNull(result);
        Assert.assertEquals(
                result.getResultCode(),
                com.cimb.vietnammobile.tool.promotion.core.configuration.Constants.RESULT_CODE_COMMUNICATION_ERROR);
    }

    @Test
    public void topup_shouldProduceResultCodeOfCommunicationError_whenTopupReturnsUnknownHttpStatus() {
        //GIVEN: mockedAPIs is TOPUP_RETURN_UNKNOWN_HTTP_STATUS
        transitMockedAPIStateTo(State.TOPUP_RETURN_UNKNOWN_HTTP_STATUS);

        //WHEN: call topup
        TopupResult result = topupService.topup(topupRequest());

        //THEN: a topupResult is returned
        Assert.assertNotNull(result);
        Assert.assertEquals(
                result.getResultCode(),
                com.cimb.vietnammobile.tool.promotion.core.configuration.Constants.RESULT_CODE_COMMUNICATION_ERROR);
    }

    @Test
    public void topup_shouldProduceResultCodeOfCommunicationError_whenTopupReturnsEmptyInternalServerError() {
        //GIVEN: mockedAPIs is TOPUP_RETURN_EMPTY_INTERNAL_SERVER_ERROR
        transitMockedAPIStateTo(State.TOPUP_RETURN_EMPTY_INTERNAL_SERVER_ERROR);

        //WHEN: call topup
        TopupResult result = topupService.topup(topupRequest());

        //THEN: a topupResult is returned
        Assert.assertNotNull(result);
        Assert.assertEquals(
                result.getResultCode(),
                com.cimb.vietnammobile.tool.promotion.core.configuration.Constants.RESULT_CODE_COMMUNICATION_ERROR);
    }

    @Test
    public void topup_shouldProduceResultCodeOfProcessingError_whenTopupReturnsUnknownReturnCodeInInternalServerError() {
        //GIVEN: mockedAPIs is TOPUP_RETURN_UNKNOWN_RESULT_CODE_IN_INTERNAL_SERVER_ERROR
        transitMockedAPIStateTo(State.TOPUP_RETURN_UNKNOWN_RESULT_CODE_IN_INTERNAL_SERVER_ERROR);

        //WHEN: call topup
        TopupResult result = topupService.topup(topupRequest());

        //THEN: a topupResult is returned
        Assert.assertNotNull(result);
        Assert.assertEquals(
                result.getResultCode(),
                com.cimb.vietnammobile.tool.promotion.core.configuration.Constants.RESULT_CODE_COMMUNICATION_ERROR);
    }

    @Test
    public void topup_shouldNotProduceResultCodeOfSuccess_whenTopupReturnsUnknownReturnCodeInOk() {
        //GIVEN: mockedAPIs is TOPUP_RETURN_UNKNOWN_RESULT_CODE_IN_OK
        transitMockedAPIStateTo(State.TOPUP_RETURN_UNKNOWN_RESULT_CODE_IN_OK);

        //WHEN: call topup
        TopupResult result = topupService.topup(topupRequest());

        //THEN: a topupResult is returned
        Assert.assertNotNull(result);
        Assert.assertNotEquals(
                result.getResultCode(),
                com.cimb.vietnammobile.tool.promotion.core.configuration.Constants.RESULT_CODE_SUCCESS);
    }

    @Test
    public void topup_shouldProduceResultCodeOfSuccessAndRemark_whenTopupReturnsStrangeValuesReturnCodeInOk() {
        //GIVEN: mockedAPIs is TOPUP_RETURN_STRANGE_VALUES_IN_OK
        transitMockedAPIStateTo(State.TOPUP_RETURN_STRANGE_VALUES_IN_OK);

        //WHEN: call topup
        TopupResult result = topupService.topup(topupRequest());

        //THEN: a topupResult is returned
        Assert.assertNotNull(result);
        Assert.assertEquals(
                result.getResultCode(),
                com.cimb.vietnammobile.tool.promotion.core.configuration.Constants.RESULT_CODE_SUCCESS);
        Assert.assertNotEquals(result.getResultMessage(), StringUtils.EMPTY);
    }

    @Test
    public void topup_shouldProduceResultCodeOfTransactionInProcess_whenTopupReturnsResultCodeOfTransactionInProcess() {
        //GIVEN: mockedAPIs is TOPUP_RETURN_TRANSACTION_IN_PROCESS
        transitMockedAPIStateTo(State.TOPUP_RETURN_TRANSACTION_IN_PROCESS);

        //WHEN: call topup
        TopupResult result = topupService.topup(topupRequest());

        //THEN: a topupResult is returned
        Assert.assertNotNull(result);
        Assert.assertEquals(
                result.getResultCode(),
                com.cimb.vietnammobile.tool.promotion.core.configuration.Constants.RESULT_CODE_TRANSACTION_IN_PROCESS);
    }

    @Test
    public void topup_shouldProduceResultCodeOfInvalidToken_whenTopupReturnsResultCodeOfInvalidToken() {
        //GIVEN: mockedAPIs is TOPUP_RETURN_INVALID_TOKEN
        transitMockedAPIStateTo(State.TOPUP_RETURN_INVALID_TOKEN);

        //WHEN: call topup
        TopupResult result = topupService.topup(topupRequest());

        //THEN: a topupResult is returned
        Assert.assertNotNull(result);
        Assert.assertEquals(
                result.getResultCode(),
                com.cimb.vietnammobile.tool.promotion.core.configuration.Constants.RESULT_CODE_INVALID_TOKEN);
    }

    private TopupRequest topupRequest() {
        return TopupRequest
                .newBuilder()
                .withPhoneNumber(Constants.SAMPLE_PHONE_NUMBER)
                .withTransactionId(Constants.SAMPLE_TRANSACTION_ID)
                .build();
    }
}
