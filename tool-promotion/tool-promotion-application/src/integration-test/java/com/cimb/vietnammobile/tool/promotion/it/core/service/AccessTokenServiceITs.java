package com.cimb.vietnammobile.tool.promotion.it.core.service;

import com.cimb.vietnammobile.tool.promotion.core.configuration.CoreConfiguration;
import com.cimb.vietnammobile.tool.promotion.core.service.AccessTokenService;
import com.cimb.vietnammobile.tool.promotion.core.service.exception.AccessTokenCommunicatingException;
import com.cimb.vietnammobile.tool.promotion.it.BaseIT;
import com.cimb.vietnammobile.tool.promotion.it.mocked.api.statemachine.State;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:application-it.properties")
@SpringBootTest(classes = {
        CoreConfiguration.class
})
public class AccessTokenServiceITs extends BaseIT {
    @Autowired
    private AccessTokenService accessTokenService;

    @Test
    public void bean_shouldBeInitiated_Successfully() {
        Assert.assertNotNull(accessTokenService);
    }

    @Test
    public void getAccessToken_shouldReturnToken_whenMockedAPIsAreNormal() {
        //GIVEN: state of mockedAPI is NORMAL
        super.transitMockedAPIStateTo(State.NORMAL);

        //WHEN: call getAccessToken
        String accessToken = accessTokenService.getAccessToken();

        //THEN: an accessToken is returned
        Assert.assertNotNull(accessToken);
    }

    @Test(expected = AccessTokenCommunicatingException.class)
    public void getAccessToken_shouldThrowException_whenMockedAPIsReturnUnknownHttpStatus() {
        //GIVEN: state of mockedAPI is LOGIN_RETURN_UNKNOWN_HTTP_STATUS
        transitMockedAPIStateTo(State.LOGIN_RETURN_UNKNOWN_HTTP_STATUS);

        //WHEN: call getAccessToken
        accessTokenService.getAccessToken();

        //THEN: an exception is thrown
    }

    @Test(expected = AccessTokenCommunicatingException.class)
    public void getAccessToken_shouldThrowException_whenMockedAPIsReturnEmptyInternalServerError() {
        //GIVEN: state of mockedAPI is LOGIN_RETURNED_EMPTY_INTERNAL_SERVER_ERROR
        transitMockedAPIStateTo(State.LOGIN_RETURNED_EMPTY_INTERNAL_SERVER_ERROR);

        //WHEN: call getAccessToken
        accessTokenService.getAccessToken();

        //THEN: an exception is thrown
    }
}
