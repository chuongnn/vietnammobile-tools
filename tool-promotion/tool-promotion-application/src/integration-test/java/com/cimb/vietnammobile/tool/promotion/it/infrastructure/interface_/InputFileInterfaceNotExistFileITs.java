package com.cimb.vietnammobile.tool.promotion.it.infrastructure.interface_;

import com.cimb.vietnammobile.tool.promotion.infrastructure.configuration.InfraStructureConfiguration;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.InputFileInterface;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.exception.InputFileAccessingException;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.properties.InputFileInterfaceProperties;
import com.cimb.vietnammobile.tool.promotion.it.BaseIT;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:application-it.properties")
@SpringBootTest(
        classes = {
                InfraStructureConfiguration.class
        },
        properties = {
                "vietnammobile.interface.input-file.file-name=notExistFile.csv"
        })
public class InputFileInterfaceNotExistFileITs extends BaseIT {
    @Autowired
    private InputFileInterfaceProperties inputFileInterfaceProperties;
    @Autowired
    private InputFileInterface inputFileInterface;

    @Test(expected = InputFileAccessingException.class)
    public void inputFile_shouldThrowException_afterProcessNotExistFile() {
        //GIVEN: a not exist file
        Assert.assertNotNull(inputFileInterfaceProperties.getFileName());

        //WHEN: call inputFile
        inputFileInterface.inputFile();

        //THEN: an exception is thrown
    }
}
