package com.cimb.vietnammobile.tool.promotion.it.core.service;

import com.cimb.vietnammobile.tool.promotion.core.configuration.CoreConfiguration;
import com.cimb.vietnammobile.tool.promotion.core.service.AccessTokenService;
import com.cimb.vietnammobile.tool.promotion.core.service.exception.AccessTokenCommunicatingException;
import com.cimb.vietnammobile.tool.promotion.it.BaseIT;
import com.cimb.vietnammobile.tool.promotion.it.mocked.api.statemachine.State;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:application-it.properties")
@SpringBootTest(
        classes = {
                CoreConfiguration.class
        },
        properties = {
                "vietnammobile.communication.login.appid=wrongAppId"
        })
public class AccessTokenServiceWrongAppidITs extends BaseIT {
    @Autowired
    private AccessTokenService accessTokenService;

    @Test(expected = AccessTokenCommunicatingException.class)
    public void getAccessToken_shouldThrowException_afterProcessingWrongAppid() {
        //GIVEN: wrong appId and mockedAPI is normal
        transitMockedAPIStateTo(State.NORMAL);

        //WHEN: call getAccessToken
        accessTokenService.getAccessToken();

        //THEN: an exception is thrown
    }
}
