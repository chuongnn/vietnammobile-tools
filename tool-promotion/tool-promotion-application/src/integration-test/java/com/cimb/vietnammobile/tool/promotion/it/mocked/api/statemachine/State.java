package com.cimb.vietnammobile.tool.promotion.it.mocked.api.statemachine;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import org.apache.commons.lang3.StringUtils;

public enum State {
    NORMAL("normal"),
    LOGIN_RETURN_UNKNOWN_HTTP_STATUS("loginReturnUnknownHttpStatus"),
    LOGIN_RETURNED_EMPTY_INTERNAL_SERVER_ERROR("loginReturnEmptyInternalServerError"),
    TOPUP_RETURN_UNKNOWN_HTTP_STATUS("topupReturnUnknownHttpStatus"),
    TOPUP_RETURN_EMPTY_INTERNAL_SERVER_ERROR("topupReturnEmptyInternalServerError"),
    TOPUP_RETURN_UNKNOWN_RESULT_CODE_IN_INTERNAL_SERVER_ERROR("topupReturnUnknownResultCodeInInternalServerError"),
    TOPUP_RETURN_UNKNOWN_RESULT_CODE_IN_OK("topupReturnUnknownResultCodeInOk"),
    TOPUP_RETURN_STRANGE_VALUES_IN_OK("topupReturnStrangeValuesInOk"),
    TOPUP_RETURN_TRANSACTION_IN_PROCESS("topupReturnTransactionInProcess"),
    TOPUP_RETURN_INVALID_TOKEN("topupReturnInvalidToken")
    ;

    private final String value;

    State(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value;
    }

    @JsonValue
    public String getValue() {
        return value;
    }

    @JsonCreator
    public static State fromValue(String value) {
        for (int i = 0; i < values().length; i++) {
            State e = values()[i];
            if (StringUtils.equalsIgnoreCase(e.getValue(), value)) return e;
        }
        return null;
    }
}
