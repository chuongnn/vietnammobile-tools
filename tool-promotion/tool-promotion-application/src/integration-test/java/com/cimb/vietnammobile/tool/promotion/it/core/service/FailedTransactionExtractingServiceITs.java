package com.cimb.vietnammobile.tool.promotion.it.core.service;

import com.cimb.vietnammobile.tool.promotion.core.configuration.CoreConfiguration;
import com.cimb.vietnammobile.tool.promotion.core.model.TopupRequest;
import com.cimb.vietnammobile.tool.promotion.core.service.FailedTransactionExtractingService;
import com.cimb.vietnammobile.tool.promotion.it.BaseIT;
import com.cimb.vietnammobile.tool.promotion.it.Constants;
import com.cimb.vietnammobile.tools.util.FileUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;

import java.nio.file.Path;
import java.util.List;

@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:application-it.properties")
@SpringBootTest(
        classes = {
                CoreConfiguration.class
        })
public class FailedTransactionExtractingServiceITs extends BaseIT {
    @Autowired
    private FailedTransactionExtractingService failedTransactionExtractingService;

    @Test
    public void bootstrap_shouldInitiateService_Successfully() {
        Assert.assertNotNull(failedTransactionExtractingService);
    }

    @Test
    public void extract_shouldReturnAnEmptyList_afterProcessANoFailedOutputFile() throws Exception {
        //GIVEN: a no failed outputFile
        Path outputFile = FileUtils.existingReadableFilePathOf(Constants.SAMPLE_OUTPUT_FILE_NO_FAILED);

        //WHEN: call extract with above outputFile
        List<TopupRequest> topupRequests = failedTransactionExtractingService.extract(outputFile);

        //THEN: an empty list is returned
        Assert.assertNotNull(topupRequests);
        Assert.assertTrue(CollectionUtils.isEmpty(topupRequests));
    }

    @Test
    public void extract_shouldReturnANoneEmptyList_afterProcessASampleOutputFile() throws Exception {
        //GIVEN: a no sample outputFile
        Path outputFile = FileUtils.existingReadableFilePathOf(Constants.SAMPLE_OUTPUT_FILE);

        //WHEN: call extract with above outputFile
        List<TopupRequest> topupRequests = failedTransactionExtractingService.extract(outputFile);

        //THEN: an empty list is returned
        Assert.assertNotNull(topupRequests);
        Assert.assertFalse(CollectionUtils.isEmpty(topupRequests));
    }
}
