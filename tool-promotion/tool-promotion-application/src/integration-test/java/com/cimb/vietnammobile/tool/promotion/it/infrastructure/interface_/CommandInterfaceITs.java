package com.cimb.vietnammobile.tool.promotion.it.infrastructure.interface_;

import com.cimb.vietnammobile.tool.promotion.infrastructure.configuration.InfraStructureConfiguration;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.CommandInterface;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.properties.CommandInterfaceProperties;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.value.Command;
import com.cimb.vietnammobile.tool.promotion.it.BaseIT;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:application-it.properties")
@SpringBootTest(
        classes = {
                InfraStructureConfiguration.class
        },
        properties = {
                "vietnammobile.interface.command.command=run"
        })
public class CommandInterfaceITs extends BaseIT {
    @Autowired
    private CommandInterface commandInterface;
    @Autowired
    private CommandInterfaceProperties commandInterfaceProperties;

    @Test
    public void bootstrap_shouldInitiateInterface_Successfully() {
        Assert.assertNotNull(commandInterface);
    }

    @Test
    public void command_shouldReturnACommandObject_afterProcessingAValidCommand() {
        //GIVEN: a valid command
        Assert.assertNotNull(commandInterfaceProperties.getCommand());

        //WHEN: call command
        Command command = commandInterface.command();

        //THEN: a command object is return
        Assert.assertEquals(command, commandInterfaceProperties.getCommand());
    }
}
