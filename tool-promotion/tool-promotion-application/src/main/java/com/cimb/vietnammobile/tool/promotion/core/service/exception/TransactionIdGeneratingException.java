package com.cimb.vietnammobile.tool.promotion.core.service.exception;

public class TransactionIdGeneratingException extends RuntimeException {
    private static final String MESSAGE_TEMPLATE = "Error while generating transactionId for phoneNumber=%s";

    public TransactionIdGeneratingException(String phoneNumber, Throwable throwable) {
        super(
                String.format(MESSAGE_TEMPLATE, phoneNumber),
                throwable);
    }
}
