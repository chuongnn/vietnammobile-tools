package com.cimb.vietnammobile.tool.promotion.infrastructure.rest.configuration;

import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContextBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.ResourceUtils;
import org.springframework.web.client.RestTemplate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import java.io.InputStream;
import java.net.URI;
import java.security.KeyStore;

@Configuration
@EnableConfigurationProperties(value = {
        ConnectionProperties.class
})
public class RestClientConfiguration {
    private static final String URL_TEMPLATE = "%s://%s:%s/%s";
    private static final String HTTP_KEYWORD = "http";
    private static final String HTTPS_KEYWORD = "https";
    private static final String CONTENT_TYPE_KEYWORD = "Content-Type";
    private static final String ACCEPT_KEYWORD = "Accept";

    @Autowired
    private ConnectionProperties connectionProperties;

    @Bean
    public URI accessTokenUri() {
        return URI.create(String.format(URL_TEMPLATE,
                connectionProperties.getProtocol() == ConnectionProperties.Protocol.HTTP
                        ? HTTP_KEYWORD
                        : HTTPS_KEYWORD,
                connectionProperties.getHost(),
                connectionProperties.getPort(),
                connectionProperties.getUri().getLogin()));
    }

    @Bean
    public URI topupUri() {
        return URI.create(String.format(URL_TEMPLATE,
                connectionProperties.getProtocol() == ConnectionProperties.Protocol.HTTP
                        ? HTTP_KEYWORD
                        : HTTPS_KEYWORD,
                connectionProperties.getHost(),
                connectionProperties.getPort(),
                connectionProperties.getUri().getTopup()));
    }

    @Bean
    public RestTemplate accessTokenRestTemplate(RestTemplateBuilder restTemplateBuilder) {
        return restTemplateBuilder
                .additionalInterceptors(
                        interceptor(
                                connectionProperties.getContentType().getLogin(),
                                connectionProperties.getAccept().getLogin()
                        )
                ).requestFactory(
                        () -> requestFactory(
                                connectionProperties.getConnectionRequestTimeout().getLogin(),
                                connectionProperties.getConnectTimeout().getLogin(),
                                connectionProperties.getReadTimeout().getLogin(),
                                httpClient(
                                        connectionManager(
                                                connectionProperties.getMaxTotal().getLogin(),
                                                connectionProperties.getDefaultMaxPerRoute().getLogin(),
                                                registry(
                                                        sslSocketFactory(
                                                                sslContext(
                                                                        trustStore(
                                                                                connectionProperties.getSsl().getTrustStoreType().getLogin(),
                                                                                connectionProperties.getSsl().getTrustStoreFilePath().getLogin(),
                                                                                connectionProperties.getSsl().getTrustStorePassword().getLogin()
                                                                        )
                                                                ),
                                                                hostnameVerifier()
                                                        )
                                                )
                                        )
                                )
                        )
                ).build();
    }

    @Bean
    public RestTemplate topupRestTemplate(RestTemplateBuilder restTemplateBuilder) {
        return restTemplateBuilder
                .additionalInterceptors(
                        interceptor(
                                connectionProperties.getContentType().getTopup(),
                                connectionProperties.getAccept().getTopup()
                        )
                ).requestFactory(
                        () -> requestFactory(
                                connectionProperties.getConnectionRequestTimeout().getTopup(),
                                connectionProperties.getConnectTimeout().getTopup(),
                                connectionProperties.getReadTimeout().getTopup(),
                                httpClient(
                                        connectionManager(
                                                connectionProperties.getMaxTotal().getTopup(),
                                                connectionProperties.getDefaultMaxPerRoute().getTopup(),
                                                registry(
                                                        sslSocketFactory(
                                                                sslContext(
                                                                        trustStore(
                                                                                connectionProperties.getSsl().getTrustStoreType().getTopup(),
                                                                                connectionProperties.getSsl().getTrustStoreFilePath().getTopup(),
                                                                                connectionProperties.getSsl().getTrustStorePassword().getTopup()
                                                                        )
                                                                ),
                                                                hostnameVerifier()
                                                        )
                                                )
                                        )
                                )
                        )
                ).build();
    }

    private ClientHttpRequestInterceptor interceptor(
            String contentType,
            String accept) {
        return
                (request, body, execution) ->
                {
                    request.getHeaders().set(CONTENT_TYPE_KEYWORD, contentType);
                    request.getHeaders().set(ACCEPT_KEYWORD, accept);
                    return execution.execute(request, body);
                };
    }

    private HttpComponentsClientHttpRequestFactory requestFactory(
            int connectionRequestTimeout,
            int connectTimeout,
            int readTimeout,
            CloseableHttpClient httpClient
    ) {
        HttpComponentsClientHttpRequestFactory factory =
                new HttpComponentsClientHttpRequestFactory(httpClient);
        factory.setConnectionRequestTimeout(connectionRequestTimeout);
        factory.setConnectTimeout(connectTimeout);
        factory.setReadTimeout(readTimeout);
        return factory;
    }

    private CloseableHttpClient httpClient(PoolingHttpClientConnectionManager connectionManager) {
        return HttpClients
                .custom()
                .setConnectionManager(connectionManager)
                .build();
    }

    private PoolingHttpClientConnectionManager connectionManager(
            int maxTotal,
            int defaultMaxPerRoute,
            Registry<ConnectionSocketFactory> registry) {
        PoolingHttpClientConnectionManager manager = connectionProperties.getProtocol() == ConnectionProperties.Protocol.HTTP
                ? new PoolingHttpClientConnectionManager()
                : new PoolingHttpClientConnectionManager(registry);
        manager.setMaxTotal(maxTotal);
        manager.setDefaultMaxPerRoute(defaultMaxPerRoute);
        return manager;
    }

    private Registry<ConnectionSocketFactory> registry(SSLConnectionSocketFactory sslSocketFactory) {
        return RegistryBuilder.<ConnectionSocketFactory>create()
                .register(HTTPS_KEYWORD, sslSocketFactory)
                .build();
    }

    private SSLConnectionSocketFactory sslSocketFactory(
            SSLContext sslContext,
            HostnameVerifier hostnameVerifier) {
        return new SSLConnectionSocketFactory(sslContext, hostnameVerifier);
    }

    private SSLContext sslContext(KeyStore trustStore) {
        try {
            return new SSLContextBuilder()
                    .loadTrustMaterial(trustStore, new TrustSelfSignedStrategy())
                    .build();
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }

    private HostnameVerifier hostnameVerifier() {
        return new NoopHostnameVerifier();
    }

    private KeyStore trustStore(String storeType, String storeFilePath, String storePassword) {
        try (InputStream inputStream = ResourceUtils.getURL(storeFilePath).openStream()) {
            final KeyStore store = KeyStore.getInstance(storeType);
            store.load(inputStream, storePassword.toCharArray());
            return store;
        } catch (Throwable throwable) {
            throw new RuntimeException(throwable);
        }
    }
}
