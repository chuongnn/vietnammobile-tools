package com.cimb.vietnammobile.tool.promotion.application.exception;

public class RetryRunnerException extends RuntimeException {
    private static final String MESSAGE = "Error while executing RetryRunner";

    public RetryRunnerException(Throwable throwable) {
        super(MESSAGE, throwable);
    }
}
