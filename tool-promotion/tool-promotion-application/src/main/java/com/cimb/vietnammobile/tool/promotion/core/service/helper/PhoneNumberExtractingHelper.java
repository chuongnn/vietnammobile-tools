package com.cimb.vietnammobile.tool.promotion.core.service.helper;

import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.properties.InputFileInterfaceProperties;
import com.univocity.parsers.common.record.Record;
import com.univocity.parsers.csv.CsvParser;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

public class PhoneNumberExtractingHelper {
    private CsvParser csvParser;
    private InputFileInterfaceProperties inputFileInterfaceProperties;

    private PhoneNumberExtractingHelper(Builder builder) {
        csvParser = builder.csvParser;
        inputFileInterfaceProperties = builder.inputFileInterfaceProperties;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public List<String> extract(Path inputFile) {
        List<Record> records = csvParser.parseAllRecords(inputFile.toFile(), StandardCharsets.UTF_8);
        if (inputFileInterfaceProperties.getContainsTrailerLine()) {
            records.remove(records.size() - 1);
        }
        return records.stream()
                .map(this::extractPhoneNumber)
                .collect(Collectors.toList());
    }

    private String extractPhoneNumber(Record record) {
        return record.getString(inputFileInterfaceProperties.getPhoneNumberColumnName());
    }

    public static final class Builder {
        private CsvParser csvParser;
        private InputFileInterfaceProperties inputFileInterfaceProperties;

        private Builder() {
        }

        public Builder withCsvParser(CsvParser csvParser) {
            this.csvParser = csvParser;
            return this;
        }

        public Builder withInputFileInterfaceProperties(InputFileInterfaceProperties inputFileInterfaceProperties) {
            this.inputFileInterfaceProperties = inputFileInterfaceProperties;
            return this;
        }

        public PhoneNumberExtractingHelper build() {
            return new PhoneNumberExtractingHelper(this);
        }
    }
}
