package com.cimb.vietnammobile.tool.promotion.core.service.helper.configuration;

import com.cimb.vietnammobile.tool.promotion.core.service.helper.FailedTransactionExtractingHelper;
import com.cimb.vietnammobile.tool.promotion.core.service.helper.KeyhashHelper;
import com.cimb.vietnammobile.tool.promotion.core.service.helper.LoginHelper;
import com.cimb.vietnammobile.tool.promotion.core.service.helper.PhoneNumberExtractingHelper;
import com.cimb.vietnammobile.tool.promotion.core.service.helper.TopupResultWritingHelper;
import com.cimb.vietnammobile.tool.promotion.core.service.properties.LoginProperties;
import com.cimb.vietnammobile.tool.promotion.core.service.properties.ServicePropertiesConfiguration;
import com.cimb.vietnammobile.tool.promotion.infrastructure.configuration.InfraStructureConfiguration;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.OutputFileInterface;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.properties.InputFileInterfaceProperties;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.properties.OutputFileInterfaceProperties;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import com.univocity.parsers.csv.CsvWriter;
import com.univocity.parsers.csv.CsvWriterSettings;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(value = {
        InfraStructureConfiguration.class,
        ServicePropertiesConfiguration.class
})
public class ServiceHelperConfiguration {
    private static final String CSV_WRITER_SETTINGS_NULL_VALUE = "null";
    private static final String CSV_WRITER_SETTINGS_EMPTY_VALUE = StringUtils.EMPTY;
    private static final boolean CSV_WRITER_SETTINGS_QUOTE_ALL_FIELDS = true;
    private static final boolean CSV_WRITER_SETTINGS_QUOTE_ESCAPING_ENABLED = true;
    private static final boolean CSV_WRITER_SETTINGS_HEADER_WRITING_ENABLED = true;
    private static final String CSV_WRITER_SETTINGS_LINE_SEPARATOR = StringUtils.LF;
    private static final boolean CSV_PARSER_SETTINGS_HEADER_EXTRACTION_ENABLED = true;

    @Autowired
    private LoginProperties loginProperties;
    @Autowired
    private InputFileInterfaceProperties inputFileInterfaceProperties;
    @Autowired
    private OutputFileInterfaceProperties outputFileInterfaceProperties;
    @Autowired
    private OutputFileInterface outputFileInterface;

    @Bean
    public LoginHelper loginHelper() {
        return LoginHelper
                .newBuilder()
                .withUsername(loginProperties.getUsername())
                .withKeyhash(keyhashHelper().getKeyhash())
                .withAppid(loginProperties.getAppid())
                .build();
    }

    @Bean
    public TopupResultWritingHelper topupResultWritingHelper() {
        return TopupResultWritingHelper
                .newBuilder()
                .withCsvWriter(csvWriterForTopupResultWritingHelper())
                .build();
    }

    @Bean
    public PhoneNumberExtractingHelper phoneNumberExtractingHelper() {
        return PhoneNumberExtractingHelper
                .newBuilder()
                .withCsvParser(csvParserForPhoneNumberExtractingHelper())
                .withInputFileInterfaceProperties(inputFileInterfaceProperties)
                .build();
    }

    @Bean
    public FailedTransactionExtractingHelper failedTransactionExtractingHelper() {
        return FailedTransactionExtractingHelper
                .newBuilder()
                .withOutputFileInterfaceProperties(outputFileInterfaceProperties)
                .withCsvParser(csvParserForFailedTransactionExtractingHelper())
                .build();
    }

    private KeyhashHelper keyhashHelper() {
        return KeyhashHelper
                .newBuilder()
                .withPassword(loginProperties.getPassword())
                .withAesKey(loginProperties.getAesKey())
                .build();
    }

    private CsvWriter csvWriterForTopupResultWritingHelper() {
        return new CsvWriter(
                outputFileInterface.outputFile().toFile(),
                csvWriterSettingsForTopupResultWritingHelper()
        );
    }

    private CsvWriterSettings csvWriterSettingsForTopupResultWritingHelper() {
        CsvWriterSettings settings = new CsvWriterSettings();
        settings.setNullValue(CSV_WRITER_SETTINGS_NULL_VALUE);
        settings.setEmptyValue(CSV_WRITER_SETTINGS_EMPTY_VALUE);
        settings.setQuoteAllFields(CSV_WRITER_SETTINGS_QUOTE_ALL_FIELDS);
        settings.setQuoteEscapingEnabled(CSV_WRITER_SETTINGS_QUOTE_ESCAPING_ENABLED);
        settings.setHeaders(outputFileInterfaceProperties.getHeaders());
        settings.setHeaderWritingEnabled(CSV_WRITER_SETTINGS_HEADER_WRITING_ENABLED);
        settings.getFormat().setLineSeparator(CSV_WRITER_SETTINGS_LINE_SEPARATOR);
        return settings;
    }

    private CsvParser csvParserForPhoneNumberExtractingHelper() {
        return new CsvParser(csvParserSettingsForPhoneNumberExtractingHelper());
    }

    private CsvParser csvParserForFailedTransactionExtractingHelper() {
        return new CsvParser(csvParserSettingsForFailedTransactionExtractingHelper());
    }

    private CsvParserSettings csvParserSettingsForPhoneNumberExtractingHelper() {
        return csvParserSettings();
    }

    private CsvParserSettings csvParserSettingsForFailedTransactionExtractingHelper() {
        return csvParserSettings();
    }

    private CsvParserSettings csvParserSettings() {
        CsvParserSettings settings = new CsvParserSettings();
        settings.setHeaderExtractionEnabled(CSV_PARSER_SETTINGS_HEADER_EXTRACTION_ENABLED);
        return settings;
    }
}
