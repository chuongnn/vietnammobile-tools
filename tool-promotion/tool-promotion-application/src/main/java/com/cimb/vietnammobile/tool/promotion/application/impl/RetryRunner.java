package com.cimb.vietnammobile.tool.promotion.application.impl;

import com.cimb.vietnammobile.tool.promotion.application.Runner;
import com.cimb.vietnammobile.tool.promotion.application.exception.RetryRunnerException;
import com.cimb.vietnammobile.tool.promotion.core.model.TopupRequest;
import com.cimb.vietnammobile.tool.promotion.core.service.FailedTransactionExtractingService;
import com.cimb.vietnammobile.tool.promotion.core.service.TopupResultWritingService;
import com.cimb.vietnammobile.tool.promotion.core.service.TopupService;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.InputFileInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.Optional;

public class RetryRunner implements Runner {
    private static final Logger log = LoggerFactory.getLogger(RetryRunner.class);
    private static final String LOG_TEMPLATE_RETRY_TOPUP_TOPUP_REQUEST = "Retry topup topupRequest=%s";
    private static final String LOG_TEMPLATE_ERROR_WHILE_RETRYING_TOPUP_TOPUP_REQUEST = "Error while retrying topup topupRequest=%s";

    private InputFileInterface inputFileInterface;
    private FailedTransactionExtractingService failedTransactionExtractingService;
    private TopupService topupService;
    private TopupResultWritingService topupResultWritingService;

    private RetryRunner(Builder builder) {
        inputFileInterface = builder.inputFileInterface;
        failedTransactionExtractingService = builder.failedTransactionExtractingService;
        topupService = builder.topupService;
        topupResultWritingService = builder.topupResultWritingService;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    @Override
    public void run() {
        try {
            Optional.of(inputFileInterface.inputFile())
                    .map(failedTransactionExtractingService::extract)
                    .orElse(Collections.emptyList())
                    .forEach(this::topup);
        } catch (Throwable throwable) {
            throw new RetryRunnerException(throwable);
        }
    }

    private void topup(TopupRequest topupRequest) {
        try {
            log.info(String.format(LOG_TEMPLATE_RETRY_TOPUP_TOPUP_REQUEST, topupRequest));
            Optional.of(topupRequest)
                    .map(topupService::topup)
                    .ifPresent(topupResultWritingService::write);
        } catch (Throwable throwable) {
            log.error(
                    String.format(LOG_TEMPLATE_ERROR_WHILE_RETRYING_TOPUP_TOPUP_REQUEST, topupRequest),
                    throwable);
        }
    }

    public static final class Builder {
        private InputFileInterface inputFileInterface;
        private FailedTransactionExtractingService failedTransactionExtractingService;
        private TopupService topupService;
        private TopupResultWritingService topupResultWritingService;

        private Builder() {
        }

        public Builder withInputFileInterface(InputFileInterface inputFileInterface) {
            this.inputFileInterface = inputFileInterface;
            return this;
        }

        public Builder withFailedTransactionExtractingService(FailedTransactionExtractingService failedTransactionExtractingService) {
            this.failedTransactionExtractingService = failedTransactionExtractingService;
            return this;
        }

        public Builder withTopupService(TopupService topupService) {
            this.topupService = topupService;
            return this;
        }

        public Builder withTopupResultWritingService(TopupResultWritingService topupResultWritingService) {
            this.topupResultWritingService = topupResultWritingService;
            return this;
        }

        public RetryRunner build() {
            return new RetryRunner(this);
        }
    }
}
