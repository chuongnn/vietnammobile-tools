package com.cimb.vietnammobile.tool.promotion;

import com.cimb.vietnammobile.tool.promotion.application.Runner;
import com.cimb.vietnammobile.tool.promotion.application.configuration.RunnerConfiguration;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.CommandInterface;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(value = {
        RunnerConfiguration.class
})
public class PromotionTool implements CommandLineRunner {
    private static final Logger log = LoggerFactory.getLogger(PromotionTool.class);
    private static final String LOG_TEMPLATE_ERROR_WHILE_INVOKING_THE_PROMOTION_TOOL_WITH_ARGS =
            "Error while invoking the promotionTool with args=%s";

    private final CommandInterface commandInterface;
    private final Runner runRunner;
    private final Runner retryRunner;

    @Autowired
    public PromotionTool(CommandInterface commandInterface,
                         Runner runRunner,
                         Runner retryRunner) {
        this.commandInterface = commandInterface;
        this.runRunner = runRunner;
        this.retryRunner = retryRunner;
    }

    @Override
    public void run(String... args) {
        try {
            switch (commandInterface.command()) {
                case RUN:
                    runRunner.run();
                    break;
                case RETRY:
                    retryRunner.run();
                    break;
            }
        } catch (Throwable throwable) {
            log.error(
                    String.format(LOG_TEMPLATE_ERROR_WHILE_INVOKING_THE_PROMOTION_TOOL_WITH_ARGS, StringUtils.join(args, StringUtils.SPACE)),
                    throwable);
        }
    }

    public static void main(String[] args) {
        SpringApplication.run(PromotionTool.class, args).registerShutdownHook();
    }
}
