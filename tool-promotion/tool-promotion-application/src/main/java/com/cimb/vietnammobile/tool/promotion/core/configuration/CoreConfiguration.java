package com.cimb.vietnammobile.tool.promotion.core.configuration;

import com.cimb.vietnammobile.tool.promotion.core.service.configuration.ServiceConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(value = {
        ServiceConfiguration.class
})
public class CoreConfiguration {
}
