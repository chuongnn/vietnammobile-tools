package com.cimb.vietnammobile.tool.promotion.core.service.impl;

import com.cimb.vietnammobile.tool.promotion.core.model.TopupResult;
import com.cimb.vietnammobile.tool.promotion.core.service.TopupResultWritingService;
import com.cimb.vietnammobile.tool.promotion.core.service.exception.TopupResultWritingException;
import com.cimb.vietnammobile.tool.promotion.core.service.helper.TopupResultWritingHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TopupResultWritingServiceImpl implements TopupResultWritingService {
    private static final Logger log = LoggerFactory.getLogger(TopupResultWritingServiceImpl.class);
    private static final String LOG_TEMPLATE_WRITE_TOPUP_RESULT = "Write topupResult=%s";

    private TopupResultWritingHelper topupResultWritingHelper;

    private TopupResultWritingServiceImpl(Builder builder) {
        topupResultWritingHelper = builder.topupResultWritingHelper;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    @Override
    public void write(TopupResult result) {
        try {
            log.info(String.format(LOG_TEMPLATE_WRITE_TOPUP_RESULT, result));
            topupResultWritingHelper.write(result);
        } catch (Throwable throwable) {
            throw new TopupResultWritingException(result, throwable);
        }
    }

    public static final class Builder {
        private TopupResultWritingHelper topupResultWritingHelper;

        private Builder() {
        }

        public Builder withTopupResultWritingHelper(TopupResultWritingHelper topupResultWritingHelper) {
            this.topupResultWritingHelper = topupResultWritingHelper;
            return this;
        }

        public TopupResultWritingServiceImpl build() {
            return new TopupResultWritingServiceImpl(this);
        }
    }
}
