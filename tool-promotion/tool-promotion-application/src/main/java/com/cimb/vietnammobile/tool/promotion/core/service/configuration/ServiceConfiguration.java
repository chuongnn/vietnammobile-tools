package com.cimb.vietnammobile.tool.promotion.core.service.configuration;

import com.cimb.vietnammobile.tool.promotion.core.configuration.Constants;
import com.cimb.vietnammobile.tool.promotion.core.service.AccessTokenService;
import com.cimb.vietnammobile.tool.promotion.core.service.FailedTransactionExtractingService;
import com.cimb.vietnammobile.tool.promotion.core.service.PhoneNumberExtractingService;
import com.cimb.vietnammobile.tool.promotion.core.service.TopupResultWritingService;
import com.cimb.vietnammobile.tool.promotion.core.service.TopupService;
import com.cimb.vietnammobile.tool.promotion.core.service.TransactionIdGeneratingService;
import com.cimb.vietnammobile.tool.promotion.core.service.helper.FailedTransactionExtractingHelper;
import com.cimb.vietnammobile.tool.promotion.core.service.helper.LoginHelper;
import com.cimb.vietnammobile.tool.promotion.core.service.helper.PhoneNumberExtractingHelper;
import com.cimb.vietnammobile.tool.promotion.core.service.helper.TopupResultWritingHelper;
import com.cimb.vietnammobile.tool.promotion.core.service.helper.configuration.ServiceHelperConfiguration;
import com.cimb.vietnammobile.tool.promotion.core.service.impl.AccessTokenServiceImpl;
import com.cimb.vietnammobile.tool.promotion.core.service.impl.FailedTransactionExtractingServiceImpl;
import com.cimb.vietnammobile.tool.promotion.core.service.impl.PhoneNumberExtractingServiceImpl;
import com.cimb.vietnammobile.tool.promotion.core.service.impl.TopupResultWritingServiceImpl;
import com.cimb.vietnammobile.tool.promotion.core.service.impl.TopupServiceImpl;
import com.cimb.vietnammobile.tool.promotion.core.service.impl.TransactionIdGeneratingServiceImpl;
import com.cimb.vietnammobile.tool.promotion.core.service.properties.ServicePropertiesConfiguration;
import com.cimb.vietnammobile.tool.promotion.core.service.properties.TopupProperties;
import com.cimb.vietnammobile.tool.promotion.core.service.properties.TransactionIdProperties;
import com.cimb.vietnammobile.tool.promotion.infrastructure.configuration.InfraStructureConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.TimeZone;

@Configuration
@Import(value = {
        InfraStructureConfiguration.class,
        ServicePropertiesConfiguration.class,
        ServiceHelperConfiguration.class
})
public class ServiceConfiguration {
    @Autowired
    private RestTemplate accessTokenRestTemplate;
    @Autowired
    private URI accessTokenUri;
    @Autowired
    private LoginHelper loginHelper;
    @Autowired
    private TopupProperties topupProperties;
    @Autowired
    private RestTemplate topupRestTemplate;
    @Autowired
    private URI topupUri;
    @Autowired
    private TopupResultWritingHelper topupResultWritingHelper;
    @Autowired
    private PhoneNumberExtractingHelper phoneNumberExtractingHelper;
    @Autowired
    private TransactionIdProperties transactionIdProperties;
    @Autowired
    private FailedTransactionExtractingHelper failedTransactionExtractingHelper;

    @Bean
    public AccessTokenService accessTokenService() {
        return AccessTokenServiceImpl
                .newBuilder()
                .withAccessTokenRestTemplate(accessTokenRestTemplate)
                .withAccessTokenUri(accessTokenUri)
                .withLoginHelper(loginHelper)
                .build();
    }

    @Bean
    public TopupService topupService() {
        return TopupServiceImpl
                .newBuilder()
                .withAccessTokenService(accessTokenService())
                .withTopupProperties(topupProperties)
                .withTopupRestTemplate(topupRestTemplate)
                .withTopupUri(topupUri)
                .build();
    }

    @Bean
    public TopupResultWritingService topupResultWritingService() {
        return TopupResultWritingServiceImpl
                .newBuilder()
                .withTopupResultWritingHelper(topupResultWritingHelper)
                .build();
    }

    @Bean
    public PhoneNumberExtractingService phoneNumberExtractingService() {
        return PhoneNumberExtractingServiceImpl
                .newBuilder()
                .withPhoneNumberExtractingHelper(phoneNumberExtractingHelper)
                .build();
    }

    @Bean
    public TransactionIdGeneratingService transactionIdGeneratingService() {
        return TransactionIdGeneratingServiceImpl
                .newBuilder()
                .withSimpleDateFormat(simpleDateFormatForGeneratingTransactionId())
                .withTransactionIdProperties(transactionIdProperties)
                .build();
    }

    @Bean
    public FailedTransactionExtractingService failedTransactionExtractingService() {
        return FailedTransactionExtractingServiceImpl
                .newBuilder()
                .withFailedTransactionExtractingHelper(failedTransactionExtractingHelper)
                .build();
    }

    private SimpleDateFormat simpleDateFormatForGeneratingTransactionId() {
        SimpleDateFormat format = new SimpleDateFormat(transactionIdProperties.getPattern());
        format.setTimeZone(TimeZone.getTimeZone(ZoneId.of(Constants.DEFAULT_TIMEZONE_OFFSET)));
        format.setLenient(false);
        return format;
    }
}
