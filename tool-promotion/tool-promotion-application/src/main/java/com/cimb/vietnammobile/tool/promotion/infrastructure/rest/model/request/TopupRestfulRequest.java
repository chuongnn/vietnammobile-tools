package com.cimb.vietnammobile.tool.promotion.infrastructure.rest.model.request;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class TopupRestfulRequest {
    private String isdn;
    private String token;
    private String appid;
    private String transactionid;

    public TopupRestfulRequest() {
    }

    private TopupRestfulRequest(Builder builder) {
        isdn = builder.isdn;
        token = builder.token;
        appid = builder.appid;
        transactionid = builder.transactionid;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getIsdn() {
        return isdn;
    }

    public String getToken() {
        return token;
    }

    public String getAppid() {
        return appid;
    }

    public String getTransactionid() {
        return transactionid;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)
                .append("isdn", isdn)
                .append("token", token)
                .append("appid", appid)
                .append("transactionid", transactionid)
                .toString();
    }

    public static final class Builder {
        private String isdn;
        private String token;
        private String appid;
        private String transactionid;

        private Builder() {
        }

        public Builder withIsdn(String isdn) {
            this.isdn = isdn;
            return this;
        }

        public Builder withToken(String token) {
            this.token = token;
            return this;
        }

        public Builder withAppid(String appid) {
            this.appid = appid;
            return this;
        }

        public Builder withTransactionid(String transactionid) {
            this.transactionid = transactionid;
            return this;
        }

        public TopupRestfulRequest build() {
            return new TopupRestfulRequest(this);
        }
    }
}
