package com.cimb.vietnammobile.tool.promotion.core.service.helper;

import com.cimb.vietnammobile.tool.promotion.core.configuration.Constants;
import com.cimb.vietnammobile.tool.promotion.core.model.TopupRequest;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.properties.OutputFileInterfaceProperties;
import com.univocity.parsers.common.record.Record;
import com.univocity.parsers.csv.CsvParser;

import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class FailedTransactionExtractingHelper {
    private OutputFileInterfaceProperties outputFileInterfaceProperties;
    private CsvParser csvParser;

    private FailedTransactionExtractingHelper(Builder builder) {
        outputFileInterfaceProperties = builder.outputFileInterfaceProperties;
        csvParser = builder.csvParser;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public List<TopupRequest> extract(Path outputFile) {
        return csvParser.parseAllRecords(outputFile.toFile(), StandardCharsets.UTF_8)
                .stream()
                .filter(this::isFailedTransaction)
                .map(this::topupRequestIn)
                .collect(Collectors.toList());
    }

    private Boolean isFailedTransaction(Record record) {
        return !Objects.equals(Constants.RESULT_CODE_SUCCESS, resultCodeIn(record));
    }

    private TopupRequest topupRequestIn(Record record) {
        return TopupRequest
                .newBuilder()
                .withPhoneNumber(phoneNumberIn(record))
                .withTransactionId(transactionIdIn(record))
                .build();
    }

    private Integer resultCodeIn(Record record) {
        return Integer.parseInt(record.getString(outputFileInterfaceProperties.getResultCodeColumnName()));
    }

    private String phoneNumberIn(Record record) {
        return record.getString(outputFileInterfaceProperties.getPhoneNumberColumnName());
    }

    private String transactionIdIn(Record record) {
        return record.getString(outputFileInterfaceProperties.getTransactionIdColumnName());
    }

    public static final class Builder {
        private OutputFileInterfaceProperties outputFileInterfaceProperties;
        private CsvParser csvParser;

        private Builder() {
        }

        public Builder withOutputFileInterfaceProperties(OutputFileInterfaceProperties outputFileInterfaceProperties) {
            this.outputFileInterfaceProperties = outputFileInterfaceProperties;
            return this;
        }

        public Builder withCsvParser(CsvParser csvParser) {
            this.csvParser = csvParser;
            return this;
        }

        public FailedTransactionExtractingHelper build() {
            return new FailedTransactionExtractingHelper(this);
        }
    }
}
