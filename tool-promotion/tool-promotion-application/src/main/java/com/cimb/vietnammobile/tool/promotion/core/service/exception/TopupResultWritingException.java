package com.cimb.vietnammobile.tool.promotion.core.service.exception;

import com.cimb.vietnammobile.tool.promotion.core.model.TopupResult;

public class TopupResultWritingException extends RuntimeException {
    private static final String MESSAGE_TEMPLATE = "Error while writing topupResult=%s";

    public TopupResultWritingException(TopupResult result, Throwable throwable) {
        super(
                String.format(MESSAGE_TEMPLATE, result),
                throwable);
    }
}
