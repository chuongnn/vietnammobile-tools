package com.cimb.vietnammobile.tool.promotion.core.service.exception;

import com.cimb.vietnammobile.tools.util.AbbreviationUtils;

public class KeyhashComputingException extends RuntimeException {
    private static final String MESSAGE_TEMPLATE = "Error while computing keyhash for p=%s, aesKey=%s";

    public KeyhashComputingException(String password, String aesKey, Throwable throwable) {
        super(
                String.format(
                        MESSAGE_TEMPLATE,
                        AbbreviationUtils.ofPassword(password),
                        AbbreviationUtils.ofPassword(aesKey)),
                throwable
        );
    }
}
