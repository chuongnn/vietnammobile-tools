package com.cimb.vietnammobile.tool.promotion.core.service.impl;

import com.cimb.vietnammobile.tool.promotion.core.configuration.Constants;
import com.cimb.vietnammobile.tool.promotion.core.service.AccessTokenService;
import com.cimb.vietnammobile.tool.promotion.core.service.exception.AccessTokenCommunicatingException;
import com.cimb.vietnammobile.tool.promotion.core.service.exception.AccessTokenExtractingException;
import com.cimb.vietnammobile.tool.promotion.core.service.helper.LoginHelper;
import com.cimb.vietnammobile.tool.promotion.infrastructure.rest.model.request.AccessTokenRestfulRequest;
import com.cimb.vietnammobile.tool.promotion.infrastructure.rest.model.response.AccessTokenRestfulResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.Objects;
import java.util.Optional;

public class AccessTokenServiceImpl implements AccessTokenService {
    private RestTemplate accessTokenRestTemplate;
    private URI accessTokenUri;
    private LoginHelper loginHelper;

    private AccessTokenServiceImpl(Builder builder) {
        accessTokenRestTemplate = builder.accessTokenRestTemplate;
        accessTokenUri = builder.accessTokenUri;
        loginHelper = builder.loginHelper;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    @Override
    public String getAccessToken() {
        return getAccessTokenImpl();
    }

    private String getAccessTokenImpl() {
        AccessTokenRestfulRequest restfulRequest = this.createAccessTokenRestfulRequest();
        AccessTokenRestfulResponse restfulResponse = this.postForRestfulResponse(restfulRequest);
        return this.extractAccessToken(restfulResponse);
    }

    private AccessTokenRestfulRequest createAccessTokenRestfulRequest() {
        return AccessTokenRestfulRequest
                .newBuilder()
                .withUsername(loginHelper.getUsername())
                .withKeyhash(loginHelper.getKeyhash())
                .withAppid(loginHelper.getAppid())
                .build();
    }

    private AccessTokenRestfulResponse postForRestfulResponse(AccessTokenRestfulRequest restfulRequest) {
        ResponseEntity<AccessTokenRestfulResponse> responseEntity = null;
        try {
            responseEntity = accessTokenRestTemplate.postForEntity(accessTokenUri, restfulRequest, AccessTokenRestfulResponse.class);
            return Optional.of(responseEntity)
                    .filter(_responseEntity -> Objects.equals(_responseEntity.getStatusCode(), HttpStatus.OK))
                    .map(ResponseEntity::getBody)
                    .orElseThrow(NullPointerException::new);
        } catch (HttpStatusCodeException ex) {
            throw new AccessTokenCommunicatingException(restfulRequest, ex);
        } catch (Throwable throwable) {
            throw new AccessTokenCommunicatingException(restfulRequest, responseEntity, throwable);
        }
    }

    private String extractAccessToken(AccessTokenRestfulResponse restfulResponse) {
        try {
            return Optional.of(restfulResponse)
                    .filter(_response -> Objects.equals(_response.getResultcode(), Constants.RESULT_CODE_SUCCESS))
                    .map(AccessTokenRestfulResponse::getMessage)
                    .orElseThrow(NullPointerException::new);
        } catch (Throwable throwable) {
            throw new AccessTokenExtractingException(restfulResponse, throwable);
        }
    }

    public static final class Builder {
        private RestTemplate accessTokenRestTemplate;
        private URI accessTokenUri;
        private LoginHelper loginHelper;

        private Builder() {
        }

        public Builder withAccessTokenRestTemplate(RestTemplate accessTokenRestTemplate) {
            this.accessTokenRestTemplate = accessTokenRestTemplate;
            return this;
        }

        public Builder withAccessTokenUri(URI accessTokenUri) {
            this.accessTokenUri = accessTokenUri;
            return this;
        }

        public Builder withLoginHelper(LoginHelper loginHelper) {
            this.loginHelper = loginHelper;
            return this;
        }

        public AccessTokenServiceImpl build() {
            return new AccessTokenServiceImpl(this);
        }
    }
}
