package com.cimb.vietnammobile.tool.promotion.infrastructure.interface_;

import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.value.Command;

public interface CommandInterface {
    Command command();
}
