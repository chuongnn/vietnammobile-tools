package com.cimb.vietnammobile.tool.promotion.core.service.exception;

import java.nio.file.Path;

public class FailedTransactionExtractingException extends RuntimeException {
    private static final String MESSAGE_TEMPLATE = "Error while extracting failed transactions from inputFile=%s";

    public FailedTransactionExtractingException(Path inputFile, Throwable throwable) {
        super(
                String.format(MESSAGE_TEMPLATE, inputFile),
                throwable
        );
    }
}
