package com.cimb.vietnammobile.tool.promotion.core.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class TopupResult {
    private String phoneNumber;
    private String transactionId;
    private Integer resultCode;
    private String resultMessage;
    private String remark;

    private TopupResult(Builder builder) {
        phoneNumber = builder.phoneNumber;
        transactionId = builder.transactionId;
        resultCode = builder.resultCode;
        resultMessage = builder.resultMessage;
        remark = builder.remark;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public Integer getResultCode() {
        return resultCode;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public String getRemark() {
        return remark;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)
                .append("phoneNumber", phoneNumber)
                .append("transactionId", transactionId)
                .append("resultCode", resultCode)
                .append("resultMessage", resultMessage)
                .append("remark", remark)
                .toString();
    }

    public static final class Builder {
        private String phoneNumber;
        private String transactionId;
        private Integer resultCode;
        private String resultMessage;
        private String remark;

        private Builder() {
        }

        public Builder withPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        public Builder withTransactionId(String transactionId) {
            this.transactionId = transactionId;
            return this;
        }

        public Builder withResultCode(Integer resultCode) {
            this.resultCode = resultCode;
            return this;
        }

        public Builder withResultMessage(String resultMessage) {
            this.resultMessage = resultMessage;
            return this;
        }

        public Builder withRemark(String remark) {
            this.remark = remark;
            return this;
        }

        public TopupResult build() {
            return new TopupResult(this);
        }
    }
}
