package com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.exception;

public class ComputingEffectiveOutputFileDescriptionException extends RuntimeException {
    private static final String MESSAGE_TEMPLATE =
            "Error while computing effectiveOutputFileDescription for outputFileDescription=%s";

    public ComputingEffectiveOutputFileDescriptionException(String outputFileDescription, Throwable throwable) {
        super(
                String.format(MESSAGE_TEMPLATE, outputFileDescription),
                throwable
        );
    }
}
