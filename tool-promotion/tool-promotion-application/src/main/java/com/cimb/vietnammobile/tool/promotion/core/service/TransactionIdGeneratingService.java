package com.cimb.vietnammobile.tool.promotion.core.service;

public interface TransactionIdGeneratingService {
    String transactionIdFor(String phoneNumber);
}
