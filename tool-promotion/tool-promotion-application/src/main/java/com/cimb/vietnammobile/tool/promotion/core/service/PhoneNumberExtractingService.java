package com.cimb.vietnammobile.tool.promotion.core.service;

import java.nio.file.Path;
import java.util.List;

public interface PhoneNumberExtractingService {
    List<String> extract(Path inputFile);
}
