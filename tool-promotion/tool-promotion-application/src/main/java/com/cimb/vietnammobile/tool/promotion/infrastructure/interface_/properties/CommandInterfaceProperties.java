package com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.properties;

import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.value.Command;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("vietnammobile.interface.command")
public class CommandInterfaceProperties {
    private Command command;

    public Command getCommand() {
        return command;
    }

    public void setCommand(Command command) {
        this.command = command;
    }
}
