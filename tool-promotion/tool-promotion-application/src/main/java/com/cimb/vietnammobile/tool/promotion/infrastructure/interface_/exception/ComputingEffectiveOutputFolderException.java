package com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.exception;

public class ComputingEffectiveOutputFolderException extends RuntimeException {
    private static final String MESSAGE_TEMPLATE =
            "Error while computing effective outputFolder for outputFolderDescription=%s";

    public ComputingEffectiveOutputFolderException(String outputFolderDescription, Throwable throwable) {
        super(
                String.format(MESSAGE_TEMPLATE, outputFolderDescription),
                throwable
        );
    }
}
