package com.cimb.vietnammobile.tool.promotion.core.service.exception;

import java.nio.file.Path;

public class PhoneNumberExtractingException extends RuntimeException {
    private static final String MESSAGE_TEMPLATE = "Error while extracting phone numbers from inputFile=%s";

    public PhoneNumberExtractingException(Path inputFile, Throwable throwable) {
        super(
                String.format(MESSAGE_TEMPLATE, inputFile),
                throwable);
    }
}
