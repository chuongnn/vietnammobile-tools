package com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.exception;

public class ComputingEffectiveOutputFileException extends RuntimeException {
    private static final String MESSAGE_TEMPLATE =
            "Error while computing effective outputFile for outputFolderDescription=%s and outputFileDescription=%s";

    public ComputingEffectiveOutputFileException(String outputFileDescription, String outputFolderDescription, Throwable throwable) {
        super(
                String.format(MESSAGE_TEMPLATE, outputFolderDescription, outputFileDescription),
                throwable
        );
    }
}
