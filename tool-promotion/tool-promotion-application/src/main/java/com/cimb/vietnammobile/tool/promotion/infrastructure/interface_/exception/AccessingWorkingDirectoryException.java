package com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.exception;

public class AccessingWorkingDirectoryException extends RuntimeException {
    private static final String MESSAGE_TEMPLATE =
            "Error while accessing workingDirectory with workingDirectoryDescription=%s";

    public AccessingWorkingDirectoryException(String workingDirectoryDescription, Throwable throwable) {
        super(
                String.format(MESSAGE_TEMPLATE, workingDirectoryDescription),
                throwable
        );
    }
}
