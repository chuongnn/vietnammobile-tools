package com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.exception;

public class CommandAccessingException extends RuntimeException {
    private static final String MESSAGE = "Error while accessing command";

    public CommandAccessingException(Throwable throwable) {
        super(MESSAGE, throwable);
    }
}
