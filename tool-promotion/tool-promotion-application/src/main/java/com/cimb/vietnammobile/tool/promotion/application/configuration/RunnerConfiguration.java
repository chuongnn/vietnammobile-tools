package com.cimb.vietnammobile.tool.promotion.application.configuration;

import com.cimb.vietnammobile.tool.promotion.application.Runner;
import com.cimb.vietnammobile.tool.promotion.application.impl.RetryRunner;
import com.cimb.vietnammobile.tool.promotion.application.impl.RunRunner;
import com.cimb.vietnammobile.tool.promotion.core.configuration.CoreConfiguration;
import com.cimb.vietnammobile.tool.promotion.core.service.FailedTransactionExtractingService;
import com.cimb.vietnammobile.tool.promotion.core.service.PhoneNumberExtractingService;
import com.cimb.vietnammobile.tool.promotion.core.service.TopupResultWritingService;
import com.cimb.vietnammobile.tool.promotion.core.service.TopupService;
import com.cimb.vietnammobile.tool.promotion.core.service.TransactionIdGeneratingService;
import com.cimb.vietnammobile.tool.promotion.infrastructure.configuration.InfraStructureConfiguration;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.InputFileInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(value = {
        InfraStructureConfiguration.class,
        CoreConfiguration.class
})
public class RunnerConfiguration {
    @Autowired
    private InputFileInterface inputFileInterface;
    @Autowired
    private PhoneNumberExtractingService phoneNumberExtractingService;
    @Autowired
    private FailedTransactionExtractingService failedTransactionExtractingService;
    @Autowired
    private TransactionIdGeneratingService transactionIdGeneratingService;
    @Autowired
    private TopupService topupService;
    @Autowired
    private TopupResultWritingService topupResultWritingService;

    @Bean
    public Runner runRunner() {
        return RunRunner
                .newBuilder()
                .withInputFileInterface(inputFileInterface)
                .withPhoneNumberExtractingService(phoneNumberExtractingService)
                .withTransactionIdGeneratingService(transactionIdGeneratingService)
                .withTopupService(topupService)
                .withTopupResultWritingService(topupResultWritingService)
                .build();
    }

    @Bean
    public Runner retryRunner() {
        return RetryRunner
                .newBuilder()
                .withInputFileInterface(inputFileInterface)
                .withFailedTransactionExtractingService(failedTransactionExtractingService)
                .withTopupService(topupService)
                .withTopupResultWritingService(topupResultWritingService)
                .build();
    }
}
