package com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.impl;

import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.OutputFileInterface;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.exception.AccessingWorkingDirectoryException;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.exception.ComputingEffectiveOutputFileDescriptionException;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.exception.ComputingEffectiveOutputFileException;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.exception.ComputingEffectiveOutputFolderException;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.exception.GeneratingEffectiveOutputFileNameDescriptionException;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.properties.OutputFileInterfaceProperties;
import com.cimb.vietnammobile.tools.util.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Optional;

import static com.cimb.vietnammobile.tools.util.ExceptionUtils.causeAsThrowableOf;
import static pl.touk.throwing.ThrowingFunction.unchecked;

public class OutputFileInterfaceImpl implements OutputFileInterface {
    private static final Logger log = LoggerFactory.getLogger(OutputFileInterfaceImpl.class);
    private static final String WORKING_DIRECTORY_DESCRIPTION = "";
    private static final String LOG_TEMPLATE_ACCEPT_OUT_FILE =
            "Compute outputFile for outputFolderDescription=%s, outputFileDescription=%s";

    private OutputFileInterfaceProperties outputFileInterfaceProperties;
    private SimpleDateFormat simpleDateFormat;
    private Path outputFile;

    private OutputFileInterfaceImpl(Builder builder) {
        outputFileInterfaceProperties = builder.outputFileInterfaceProperties;
        simpleDateFormat = builder.simpleDateFormat;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    @PostConstruct
    public void initiate() {
        this.outputFile = computeOutputFile();
    }

    @Override
    public Path outputFile() {
        return outputFile;
    }

    private Path computeOutputFile() {
        String outputFolderDescription = outputFileInterfaceProperties.getFolderName();
        String outputFileDescription = outputFileInterfaceProperties.getFileName();

        log.info(String.format(LOG_TEMPLATE_ACCEPT_OUT_FILE,
                outputFolderDescription,
                outputFileDescription));
        try {
            Path outputFile = computeEffectiveOutputFile(
                    computeEffectiveOutputFolder(outputFolderDescription),
                    computeEffectiveOutputFileDescription(outputFileDescription));
            Files.createDirectories(outputFile.getParent());
            return outputFile;
        } catch (Throwable cause) {
            throw new ComputingEffectiveOutputFileException(
                    outputFileDescription, outputFolderDescription, cause);
        }
    }

    private Path computeEffectiveOutputFile(Path effectiveOutputDirectory, String effectiveOutputFileDescription) {
        return Paths.get(effectiveOutputDirectory.toAbsolutePath().toString(), effectiveOutputFileDescription);
    }

    private Path computeEffectiveOutputFolder(String outputFolderDescription) {
        try {
            outputFolderDescription = StringUtils.isBlank(outputFolderDescription)
                    ? null :
                    outputFolderDescription;
            return Optional
                    .ofNullable(outputFolderDescription)
                    .map(unchecked(FileUtils::pathOf))
                    .orElseGet(this::workingDirectory);
        } catch (Throwable ex) {
            throw new ComputingEffectiveOutputFolderException(outputFolderDescription, causeAsThrowableOf(ex));
        }
    }

    private Path workingDirectory() {
        try {
            return Optional.of(WORKING_DIRECTORY_DESCRIPTION)
                    .map(unchecked(FileUtils::pathOf))
                    .orElseThrow(NullPointerException::new);
        } catch (Throwable ex) {
            throw new AccessingWorkingDirectoryException(WORKING_DIRECTORY_DESCRIPTION, causeAsThrowableOf(ex));
        }
    }

    private String computeEffectiveOutputFileDescription(String outputFileDescription) {
        try {
            return StringUtils.isBlank(outputFileDescription)
                    ? this.generateOutputFileNameDescription()
                    : outputFileDescription;
        } catch (Throwable cause) {
            throw new ComputingEffectiveOutputFileDescriptionException(outputFileDescription, cause);
        }
    }

    private String generateOutputFileNameDescription() {
        try {
            return simpleDateFormat.format(Calendar.getInstance().getTime())
                    + outputFileInterfaceProperties.getFileNamePatternSuffix();
        } catch (Throwable ex) {
            throw new GeneratingEffectiveOutputFileNameDescriptionException(causeAsThrowableOf(ex));
        }
    }

    public static final class Builder {
        private OutputFileInterfaceProperties outputFileInterfaceProperties;
        private SimpleDateFormat simpleDateFormat;

        private Builder() {
        }

        public Builder withOutputFileInterfaceProperties(OutputFileInterfaceProperties outputFileInterfaceProperties) {
            this.outputFileInterfaceProperties = outputFileInterfaceProperties;
            return this;
        }

        public Builder withSimpleDateFormat(SimpleDateFormat simpleDateFormat) {
            this.simpleDateFormat = simpleDateFormat;
            return this;
        }

        public OutputFileInterfaceImpl build() {
            return new OutputFileInterfaceImpl(this);
        }
    }
}
