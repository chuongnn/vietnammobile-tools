package com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.properties;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("vietnammobile.interface.output-file")
public class OutputFileInterfaceProperties {
    private static final String HEADERS_SEPARATOR_CHAR = ",";

    private String headers;
    private String phoneNumberColumnName;
    private String transactionIdColumnName;
    private String resultCodeColumnName;
    private String folderName;
    private String fileName;
    private String fileNamePatternSuffix;
    private String fileNamePatternDatePattern;

    public String[] getHeaders() {
        return StringUtils.split(headers, HEADERS_SEPARATOR_CHAR);
    }

    public void setHeaders(String headers) {
        this.headers = headers;
    }

    public String getPhoneNumberColumnName() {
        return phoneNumberColumnName;
    }

    public void setPhoneNumberColumnName(String phoneNumberColumnName) {
        this.phoneNumberColumnName = phoneNumberColumnName;
    }

    public String getTransactionIdColumnName() {
        return transactionIdColumnName;
    }

    public void setTransactionIdColumnName(String transactionIdColumnName) {
        this.transactionIdColumnName = transactionIdColumnName;
    }

    public String getResultCodeColumnName() {
        return resultCodeColumnName;
    }

    public void setResultCodeColumnName(String resultCodeColumnName) {
        this.resultCodeColumnName = resultCodeColumnName;
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileNamePatternDatePattern() {
        return fileNamePatternDatePattern;
    }

    public void setFileNamePatternDatePattern(String fileNamePatternDatePattern) {
        this.fileNamePatternDatePattern = fileNamePatternDatePattern;
    }

    public String getFileNamePatternSuffix() {
        return fileNamePatternSuffix;
    }

    public void setFileNamePatternSuffix(String fileNamePatternSuffix) {
        this.fileNamePatternSuffix = fileNamePatternSuffix;
    }
}
