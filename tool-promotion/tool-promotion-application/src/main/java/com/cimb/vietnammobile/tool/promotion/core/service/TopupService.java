package com.cimb.vietnammobile.tool.promotion.core.service;

import com.cimb.vietnammobile.tool.promotion.core.model.TopupRequest;
import com.cimb.vietnammobile.tool.promotion.core.model.TopupResult;

public interface TopupService {
    TopupResult topup(TopupRequest request);
}
