package com.cimb.vietnammobile.tool.promotion.core.service;

import com.cimb.vietnammobile.tool.promotion.core.model.TopupRequest;

import java.nio.file.Path;
import java.util.List;

public interface FailedTransactionExtractingService {
    List<TopupRequest> extract(Path inputFile);
}
