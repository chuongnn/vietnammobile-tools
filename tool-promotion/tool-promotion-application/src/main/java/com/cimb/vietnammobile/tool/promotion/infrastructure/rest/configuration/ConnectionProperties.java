package com.cimb.vietnammobile.tool.promotion.infrastructure.rest.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("vietnammobile.connection")
public class ConnectionProperties {
    private Protocol protocol;
    private String host;
    private int port;
    private Uri uri;
    private ContentType contentType;
    private Accept accept;
    private ConnectionRequestTimeout connectionRequestTimeout;
    private ConnectTimeout connectTimeout;
    private ReadTimeout readTimeout;
    private MaxTotal maxTotal;
    private DefaultMaxPerRoute defaultMaxPerRoute;
    private SSL ssl;

    public Protocol getProtocol() {
        return protocol;
    }

    public void setProtocol(Protocol protocol) {
        this.protocol = protocol;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    public ContentType getContentType() {
        return contentType;
    }

    public void setContentType(ContentType contentType) {
        this.contentType = contentType;
    }

    public Accept getAccept() {
        return accept;
    }

    public void setAccept(Accept accept) {
        this.accept = accept;
    }

    public ConnectionRequestTimeout getConnectionRequestTimeout() {
        return connectionRequestTimeout;
    }

    public void setConnectionRequestTimeout(ConnectionRequestTimeout connectionRequestTimeout) {
        this.connectionRequestTimeout = connectionRequestTimeout;
    }

    public ConnectTimeout getConnectTimeout() {
        return connectTimeout;
    }

    public void setConnectTimeout(ConnectTimeout connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public ReadTimeout getReadTimeout() {
        return readTimeout;
    }

    public void setReadTimeout(ReadTimeout readTimeout) {
        this.readTimeout = readTimeout;
    }

    public MaxTotal getMaxTotal() {
        return maxTotal;
    }

    public void setMaxTotal(MaxTotal maxTotal) {
        this.maxTotal = maxTotal;
    }

    public DefaultMaxPerRoute getDefaultMaxPerRoute() {
        return defaultMaxPerRoute;
    }

    public void setDefaultMaxPerRoute(DefaultMaxPerRoute defaultMaxPerRoute) {
        this.defaultMaxPerRoute = defaultMaxPerRoute;
    }

    public SSL getSsl() {
        return ssl;
    }

    public void setSsl(SSL ssl) {
        this.ssl = ssl;
    }

    public enum Protocol {
        HTTPS, HTTP
    }

    public static class Uri {
        private String login;
        private String topup;

        public String getLogin() {
            return login;
        }

        public void setLogin(String login) {
            this.login = login;
        }

        public String getTopup() {
            return topup;
        }

        public void setTopup(String topup) {
            this.topup = topup;
        }
    }

    public static class ContentType {
        private String login;
        private String topup;

        public String getLogin() {
            return login;
        }

        public void setLogin(String login) {
            this.login = login;
        }

        public String getTopup() {
            return topup;
        }

        public void setTopup(String topup) {
            this.topup = topup;
        }
    }

    public static class Accept {
        private String login;
        private String topup;

        public String getLogin() {
            return login;
        }

        public void setLogin(String login) {
            this.login = login;
        }

        public String getTopup() {
            return topup;
        }

        public void setTopup(String topup) {
            this.topup = topup;
        }
    }

    public static class ConnectionRequestTimeout {
        private int login;
        private int topup;

        public int getLogin() {
            return login;
        }

        public void setLogin(int login) {
            this.login = login;
        }

        public int getTopup() {
            return topup;
        }

        public void setTopup(int topup) {
            this.topup = topup;
        }
    }

    public static class ConnectTimeout {
        private int login;
        private int topup;

        public int getLogin() {
            return login;
        }

        public void setLogin(int login) {
            this.login = login;
        }

        public int getTopup() {
            return topup;
        }

        public void setTopup(int topup) {
            this.topup = topup;
        }
    }

    public static class ReadTimeout {
        private int login;
        private int topup;

        public int getLogin() {
            return login;
        }

        public void setLogin(int login) {
            this.login = login;
        }

        public int getTopup() {
            return topup;
        }

        public void setTopup(int topup) {
            this.topup = topup;
        }
    }

    public static class MaxTotal {
        private int login;
        private int topup;

        public int getLogin() {
            return login;
        }

        public void setLogin(int login) {
            this.login = login;
        }

        public int getTopup() {
            return topup;
        }

        public void setTopup(int topup) {
            this.topup = topup;
        }
    }

    public static class DefaultMaxPerRoute {
        private int login;
        private int topup;

        public int getLogin() {
            return login;
        }

        public void setLogin(int login) {
            this.login = login;
        }

        public int getTopup() {
            return topup;
        }

        public void setTopup(int topup) {
            this.topup = topup;
        }
    }

    public static class SSL {
        private TrustStoreType trustStoreType;
        private TrustStoreFilePath trustStoreFilePath;
        private TrustStorePassword trustStorePassword;

        public TrustStoreType getTrustStoreType() {
            return trustStoreType;
        }

        public void setTrustStoreType(TrustStoreType trustStoreType) {
            this.trustStoreType = trustStoreType;
        }

        public TrustStoreFilePath getTrustStoreFilePath() {
            return trustStoreFilePath;
        }

        public void setTrustStoreFilePath(TrustStoreFilePath trustStoreFilePath) {
            this.trustStoreFilePath = trustStoreFilePath;
        }

        public TrustStorePassword getTrustStorePassword() {
            return trustStorePassword;
        }

        public void setTrustStorePassword(TrustStorePassword trustStorePassword) {
            this.trustStorePassword = trustStorePassword;
        }
    }

    public static class TrustStoreType {
        private String login;
        private String topup;

        public String getLogin() {
            return login;
        }

        public void setLogin(String login) {
            this.login = login;
        }

        public String getTopup() {
            return topup;
        }

        public void setTopup(String topup) {
            this.topup = topup;
        }
    }

    public static class TrustStoreFilePath {
        private String login;
        private String topup;

        public String getLogin() {
            return login;
        }

        public void setLogin(String login) {
            this.login = login;
        }

        public String getTopup() {
            return topup;
        }

        public void setTopup(String topup) {
            this.topup = topup;
        }
    }

    public static class TrustStorePassword {
        private String login;
        private String topup;

        public String getLogin() {
            return login;
        }

        public void setLogin(String login) {
            this.login = login;
        }

        public String getTopup() {
            return topup;
        }

        public void setTopup(String topup) {
            this.topup = topup;
        }
    }
}
