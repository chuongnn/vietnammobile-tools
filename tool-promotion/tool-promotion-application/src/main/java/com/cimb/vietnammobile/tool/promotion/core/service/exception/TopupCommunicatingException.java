package com.cimb.vietnammobile.tool.promotion.core.service.exception;

import com.cimb.vietnammobile.tool.promotion.infrastructure.rest.model.request.TopupRestfulRequest;
import org.springframework.web.client.HttpStatusCodeException;

public class TopupCommunicatingException extends RuntimeException {
    private static final String MESSAGE_TEMPLATE = "Error while communicating topupRestfulRequest=%s, response=%s";
    private static final String RESPONSE_TEMPLATE = "%s -- %s";

    public TopupCommunicatingException(TopupRestfulRequest restfulRequest, HttpStatusCodeException ex) {
        super(
                String.format(MESSAGE_TEMPLATE,
                        restfulRequest,
                        String.format(RESPONSE_TEMPLATE,
                                ex.getStatusCode(),
                                ex.getResponseBodyAsString())),
                ex
        );
    }

    public TopupCommunicatingException(TopupRestfulRequest restfulRequest, Throwable throwable) {
        super(
                String.format(MESSAGE_TEMPLATE, restfulRequest, null),
                throwable
        );
    }
}
