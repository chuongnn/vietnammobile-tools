package com.cimb.vietnammobile.tool.promotion.core.service.exception;

import com.cimb.vietnammobile.tool.promotion.infrastructure.rest.model.request.AccessTokenRestfulRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpStatusCodeException;

public class AccessTokenCommunicatingException extends RuntimeException {
    private static final String MESSAGE_TEMPLATE = "Error while communicating accessTokenRestfulRequest: %s; httpResponse=%s";
    private static final String RESPONSE_TEMPLATE = "%s -- %s";

    public AccessTokenCommunicatingException(AccessTokenRestfulRequest restfulRequest, ResponseEntity responseEntity, Throwable throwable) {
        super(
                String.format(MESSAGE_TEMPLATE, restfulRequest, responseEntity),
                throwable
        );
    }

    public AccessTokenCommunicatingException(AccessTokenRestfulRequest restfulRequest, HttpStatusCodeException exception) {
        super(
                String.format(MESSAGE_TEMPLATE,
                        restfulRequest,
                        String.format(RESPONSE_TEMPLATE,
                                exception.getStatusCode(),
                                exception.getResponseBodyAsString())),
                exception
        );
    }
}
