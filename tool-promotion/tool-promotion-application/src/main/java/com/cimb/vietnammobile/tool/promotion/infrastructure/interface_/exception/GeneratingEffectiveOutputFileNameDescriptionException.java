package com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.exception;

public class GeneratingEffectiveOutputFileNameDescriptionException extends RuntimeException {
    private static final String MESSAGE = "Error while generating EffectiveOutputFileNameDescription";

    public GeneratingEffectiveOutputFileNameDescriptionException(Throwable throwable) {
        super(MESSAGE, throwable);
    }
}
