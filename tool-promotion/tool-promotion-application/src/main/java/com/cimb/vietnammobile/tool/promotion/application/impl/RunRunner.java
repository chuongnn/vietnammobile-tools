package com.cimb.vietnammobile.tool.promotion.application.impl;

import com.cimb.vietnammobile.tool.promotion.application.Runner;
import com.cimb.vietnammobile.tool.promotion.application.exception.RunRunnerException;
import com.cimb.vietnammobile.tool.promotion.core.model.TopupRequest;
import com.cimb.vietnammobile.tool.promotion.core.service.PhoneNumberExtractingService;
import com.cimb.vietnammobile.tool.promotion.core.service.TopupResultWritingService;
import com.cimb.vietnammobile.tool.promotion.core.service.TopupService;
import com.cimb.vietnammobile.tool.promotion.core.service.TransactionIdGeneratingService;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.InputFileInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.Optional;

public class RunRunner implements Runner {
    private static final Logger log = LoggerFactory.getLogger(RunRunner.class);
    private static final String LOG_TEMPLATE_TOPUP_PHONE_NUMBER = "Topup phoneNumber=%s";
    private static final String LOG_TEMPLATE_ERROR_WHILE_TOPUP_PHONE_NUMBER = "Error while topup phoneNumber=%s";

    private InputFileInterface inputFileInterface;
    private PhoneNumberExtractingService phoneNumberExtractingService;
    private TransactionIdGeneratingService transactionIdGeneratingService;
    private TopupService topupService;
    private TopupResultWritingService topupResultWritingService;

    private RunRunner(Builder builder) {
        inputFileInterface = builder.inputFileInterface;
        phoneNumberExtractingService = builder.phoneNumberExtractingService;
        transactionIdGeneratingService = builder.transactionIdGeneratingService;
        topupService = builder.topupService;
        topupResultWritingService = builder.topupResultWritingService;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    @Override
    public void run() {
        try {
            Optional.of(inputFileInterface.inputFile())
                    .map(phoneNumberExtractingService::extract)
                    .orElse(Collections.emptyList())
                    .forEach(this::topup);
        } catch (Throwable throwable) {
            throw new RunRunnerException(throwable);
        }
    }

    private void topup(String phoneNumber) {
        try {
            log.info(String.format(LOG_TEMPLATE_TOPUP_PHONE_NUMBER, phoneNumber));
            Optional.of(phoneNumber)
                    .map(this::topupRequestFor)
                    .map(topupService::topup)
                    .ifPresent(topupResultWritingService::write);
        } catch (Throwable throwable) {
            log.error(
                    String.format(LOG_TEMPLATE_ERROR_WHILE_TOPUP_PHONE_NUMBER, phoneNumber),
                    throwable);
        }
    }

    private TopupRequest topupRequestFor(String phoneNumber) {
        return TopupRequest
                .newBuilder()
                .withPhoneNumber(phoneNumber)
                .withTransactionId(transactionIdGeneratingService.transactionIdFor(phoneNumber))
                .build();
    }


    public static final class Builder {
        private InputFileInterface inputFileInterface;
        private PhoneNumberExtractingService phoneNumberExtractingService;
        private TransactionIdGeneratingService transactionIdGeneratingService;
        private TopupService topupService;
        private TopupResultWritingService topupResultWritingService;

        private Builder() {
        }

        public Builder withInputFileInterface(InputFileInterface inputFileInterface) {
            this.inputFileInterface = inputFileInterface;
            return this;
        }

        public Builder withPhoneNumberExtractingService(PhoneNumberExtractingService phoneNumberExtractingService) {
            this.phoneNumberExtractingService = phoneNumberExtractingService;
            return this;
        }

        public Builder withTransactionIdGeneratingService(TransactionIdGeneratingService transactionIdGeneratingService) {
            this.transactionIdGeneratingService = transactionIdGeneratingService;
            return this;
        }

        public Builder withTopupService(TopupService topupService) {
            this.topupService = topupService;
            return this;
        }

        public Builder withTopupResultWritingService(TopupResultWritingService topupResultWritingService) {
            this.topupResultWritingService = topupResultWritingService;
            return this;
        }

        public RunRunner build() {
            return new RunRunner(this);
        }
    }
}
