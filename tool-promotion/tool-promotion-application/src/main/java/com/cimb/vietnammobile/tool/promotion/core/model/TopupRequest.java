package com.cimb.vietnammobile.tool.promotion.core.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class TopupRequest {
    private String phoneNumber;
    private String transactionId;

    private TopupRequest(Builder builder) {
        phoneNumber = builder.phoneNumber;
        transactionId = builder.transactionId;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getTransactionId() {
        return transactionId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)
                .append("phoneNumber", phoneNumber)
                .append("transactionId", transactionId)
                .toString();
    }

    public static final class Builder {
        private String phoneNumber;
        private String transactionId;

        private Builder() {
        }

        public Builder withPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        public Builder withTransactionId(String transactionId) {
            this.transactionId = transactionId;
            return this;
        }

        public TopupRequest build() {
            return new TopupRequest(this);
        }
    }
}
