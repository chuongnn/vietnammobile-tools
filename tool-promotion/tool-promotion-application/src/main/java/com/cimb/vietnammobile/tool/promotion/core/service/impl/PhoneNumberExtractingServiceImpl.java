package com.cimb.vietnammobile.tool.promotion.core.service.impl;

import com.cimb.vietnammobile.tool.promotion.core.service.PhoneNumberExtractingService;
import com.cimb.vietnammobile.tool.promotion.core.service.exception.PhoneNumberExtractingException;
import com.cimb.vietnammobile.tool.promotion.core.service.helper.PhoneNumberExtractingHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;
import java.util.List;

public class PhoneNumberExtractingServiceImpl implements PhoneNumberExtractingService {
    private static final Logger log = LoggerFactory.getLogger(PhoneNumberExtractingServiceImpl.class);
    private static final String LOG_TEMPLATE_EXTRACT_PHONE_NUMBERS_FROM_INPUT_FILE =
            "Extract phone numbers from inputFile=%s";

    private PhoneNumberExtractingHelper phoneNumberExtractingHelper;

    private PhoneNumberExtractingServiceImpl(Builder builder) {
        phoneNumberExtractingHelper = builder.phoneNumberExtractingHelper;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    @Override
    public List<String> extract(Path inputFile) {
        try {
            log.info(String.format(LOG_TEMPLATE_EXTRACT_PHONE_NUMBERS_FROM_INPUT_FILE, inputFile));
            return phoneNumberExtractingHelper.extract(inputFile);
        } catch (Throwable throwable) {
            throw new PhoneNumberExtractingException(inputFile, throwable);
        }
    }

    public static final class Builder {
        private PhoneNumberExtractingHelper phoneNumberExtractingHelper;

        private Builder() {
        }

        public Builder withPhoneNumberExtractingHelper(PhoneNumberExtractingHelper phoneNumberExtractingHelper) {
            this.phoneNumberExtractingHelper = phoneNumberExtractingHelper;
            return this;
        }

        public PhoneNumberExtractingServiceImpl build() {
            return new PhoneNumberExtractingServiceImpl(this);
        }
    }
}
