package com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.exception;

public class InputFileAccessingException extends RuntimeException {
    private static final String MESSAGE_TEMPLATE = "Error while accessing inputFile=%s";

    public InputFileAccessingException(String fileName, Throwable throwable) {
        super(
                String.format(MESSAGE_TEMPLATE, fileName),
                throwable
        );
    }
}
