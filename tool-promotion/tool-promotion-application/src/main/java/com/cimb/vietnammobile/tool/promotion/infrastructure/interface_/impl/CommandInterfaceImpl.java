package com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.impl;

import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.CommandInterface;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.exception.CommandAccessingException;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.properties.CommandInterfaceProperties;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.value.Command;

import java.util.Optional;

public class CommandInterfaceImpl implements CommandInterface {
    private CommandInterfaceProperties commandInterfaceProperties;

    private CommandInterfaceImpl(Builder builder) {
        commandInterfaceProperties = builder.commandInterfaceProperties;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    @Override
    public Command command() {
        try {
            return Optional.ofNullable(commandInterfaceProperties.getCommand())
                    .orElseThrow(NullPointerException::new);
        } catch (Throwable throwable) {
            throw new CommandAccessingException(throwable);
        }
    }

    public static final class Builder {
        private CommandInterfaceProperties commandInterfaceProperties;

        private Builder() {
        }

        public Builder withCommandInterfaceProperties(CommandInterfaceProperties commandInterfaceProperties) {
            this.commandInterfaceProperties = commandInterfaceProperties;
            return this;
        }

        public CommandInterfaceImpl build() {
            return new CommandInterfaceImpl(this);
        }
    }
}
