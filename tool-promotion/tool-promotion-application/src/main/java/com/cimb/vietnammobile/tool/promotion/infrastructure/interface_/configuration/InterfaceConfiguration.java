package com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.configuration;

import com.cimb.vietnammobile.tool.promotion.core.configuration.Constants;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.CommandInterface;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.InputFileInterface;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.OutputFileInterface;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.impl.CommandInterfaceImpl;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.impl.InputFileInterfaceImpl;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.impl.OutputFileInterfaceImpl;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.properties.CommandInterfaceProperties;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.properties.InputFileInterfaceProperties;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.properties.OutputFileInterfaceProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.TimeZone;

@Configuration
public class InterfaceConfiguration {
    @Autowired
    private InputFileInterfaceProperties inputFileInterfaceProperties;
    @Autowired
    private OutputFileInterfaceProperties outputFileInterfaceProperties;
    @Autowired
    private CommandInterfaceProperties commandInterfaceProperties;

    @Bean
    public InputFileInterface inputFileInterface() {
        return InputFileInterfaceImpl
                .newBuilder()
                .withInputFileInterfaceProperties(inputFileInterfaceProperties)
                .build();
    }

    @Bean
    public OutputFileInterface outputFileInterface() {
        return OutputFileInterfaceImpl
                .newBuilder()
                .withOutputFileInterfaceProperties(outputFileInterfaceProperties)
                .withSimpleDateFormat(simpleDateFormat())
                .build();
    }

    @Bean
    public CommandInterface commandInterface() {
        return CommandInterfaceImpl
                .newBuilder()
                .withCommandInterfaceProperties(commandInterfaceProperties)
                .build();
    }

    private SimpleDateFormat simpleDateFormat() {
        SimpleDateFormat format = new SimpleDateFormat(outputFileInterfaceProperties.getFileNamePatternDatePattern());
        format.setTimeZone(TimeZone.getTimeZone(ZoneId.of(Constants.DEFAULT_TIMEZONE_OFFSET)));
        format.setLenient(false);
        return format;
    }
}
