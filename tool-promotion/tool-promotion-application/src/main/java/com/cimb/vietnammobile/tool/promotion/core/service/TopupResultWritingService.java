package com.cimb.vietnammobile.tool.promotion.core.service;

import com.cimb.vietnammobile.tool.promotion.core.model.TopupResult;

public interface TopupResultWritingService {
    void write(TopupResult result);
}
