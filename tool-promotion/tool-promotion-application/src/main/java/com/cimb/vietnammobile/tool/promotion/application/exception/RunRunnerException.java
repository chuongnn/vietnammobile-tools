package com.cimb.vietnammobile.tool.promotion.application.exception;

public class RunRunnerException extends RuntimeException {
    private static final String MESSAGE = "Error while executing RunRunner";

    public RunRunnerException(Throwable throwable) {
        super(MESSAGE, throwable);
    }
}
