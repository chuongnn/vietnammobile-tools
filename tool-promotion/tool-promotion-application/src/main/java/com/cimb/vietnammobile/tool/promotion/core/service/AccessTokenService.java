package com.cimb.vietnammobile.tool.promotion.core.service;

public interface AccessTokenService {
    String getAccessToken();
}
