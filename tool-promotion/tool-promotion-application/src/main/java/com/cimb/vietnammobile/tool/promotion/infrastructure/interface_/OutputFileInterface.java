package com.cimb.vietnammobile.tool.promotion.infrastructure.interface_;

import java.nio.file.Path;

public interface OutputFileInterface {
    Path outputFile();
}
