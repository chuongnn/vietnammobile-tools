package com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("vietnammobile.interface.input-file")
public class InputFileInterfaceProperties {
    private String phoneNumberColumnName;
    private Boolean containsTrailerLine;
    private String fileName;

    public String getPhoneNumberColumnName() {
        return phoneNumberColumnName;
    }

    public void setPhoneNumberColumnName(String phoneNumberColumnName) {
        this.phoneNumberColumnName = phoneNumberColumnName;
    }

    public Boolean getContainsTrailerLine() {
        return containsTrailerLine;
    }

    public void setContainsTrailerLine(Boolean containsTrailerLine) {
        this.containsTrailerLine = containsTrailerLine;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
