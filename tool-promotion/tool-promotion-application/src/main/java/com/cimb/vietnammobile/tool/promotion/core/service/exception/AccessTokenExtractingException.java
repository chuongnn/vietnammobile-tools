package com.cimb.vietnammobile.tool.promotion.core.service.exception;

import com.cimb.vietnammobile.tool.promotion.infrastructure.rest.model.response.AccessTokenRestfulResponse;

public class AccessTokenExtractingException extends RuntimeException {
    private static final String MESSAGE_TEMPLATE = "Error while extracting accessToken from restfulResponse: %s";

    public AccessTokenExtractingException(AccessTokenRestfulResponse restfulResponse, Throwable throwable) {
        super(
                String.format(MESSAGE_TEMPLATE, restfulResponse),
                throwable
        );
    }
}
