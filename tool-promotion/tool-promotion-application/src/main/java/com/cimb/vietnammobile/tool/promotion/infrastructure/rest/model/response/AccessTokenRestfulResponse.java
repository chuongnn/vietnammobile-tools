package com.cimb.vietnammobile.tool.promotion.infrastructure.rest.model.response;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class AccessTokenRestfulResponse {
    private String message;
    private Integer resultcode;

    public AccessTokenRestfulResponse() {
    }

    private AccessTokenRestfulResponse(Builder builder) {
        message = builder.message;
        resultcode = builder.resultcode;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getMessage() {
        return message;
    }

    public Integer getResultcode() {
        return resultcode;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)
                .append("message", message)
                .append("resultcode", resultcode)
                .toString();
    }

    public static final class Builder {
        private String message;
        private Integer resultcode;

        private Builder() {
        }

        public Builder withMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder withResultcode(Integer resultcode) {
            this.resultcode = resultcode;
            return this;
        }

        public AccessTokenRestfulResponse build() {
            return new AccessTokenRestfulResponse(this);
        }
    }
}
