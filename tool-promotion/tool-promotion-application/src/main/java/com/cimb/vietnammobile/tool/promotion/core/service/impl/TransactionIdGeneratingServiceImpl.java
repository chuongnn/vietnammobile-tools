package com.cimb.vietnammobile.tool.promotion.core.service.impl;

import com.cimb.vietnammobile.tool.promotion.core.service.TransactionIdGeneratingService;
import com.cimb.vietnammobile.tool.promotion.core.service.exception.TransactionIdGeneratingException;
import com.cimb.vietnammobile.tool.promotion.core.service.properties.TransactionIdProperties;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Optional;

public class TransactionIdGeneratingServiceImpl implements TransactionIdGeneratingService {
    private static final Logger log = LoggerFactory.getLogger(TransactionIdGeneratingServiceImpl.class);
    private static final String LOG_TEMPLATE_GENERATE_TRANSACTION_ID_FOR_PHONE_NUMBER = "Generate transactionId for phoneNumber=%s";
    private static final String TEMPLATE_TRANSACTION_ID = "%s%s";
    private static final int TRANSACTION_ID_MAX_LENGTH = 20;

    private TransactionIdProperties transactionIdProperties;
    private SimpleDateFormat simpleDateFormat;

    private TransactionIdGeneratingServiceImpl(Builder builder) {
        transactionIdProperties = builder.transactionIdProperties;
        simpleDateFormat = builder.simpleDateFormat;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    @Override
    public String transactionIdFor(String phoneNumber) {
        try {
            log.info(String.format(LOG_TEMPLATE_GENERATE_TRANSACTION_ID_FOR_PHONE_NUMBER, phoneNumber));
            return transactionIdForImpl();
        } catch (Throwable throwable) {
            throw new TransactionIdGeneratingException(phoneNumber, throwable);
        }
    }

    private String transactionIdForImpl() {
        return Optional.of(String.format(
                TEMPLATE_TRANSACTION_ID,
                transactionIdProperties.getPrefix(),
                simpleDateFormat.format(Calendar.getInstance().getTime())))
                .map(_generatedTransactionId -> StringUtils.truncate(_generatedTransactionId, TRANSACTION_ID_MAX_LENGTH))
                .orElse(StringUtils.EMPTY);
    }

    public static final class Builder {
        private TransactionIdProperties transactionIdProperties;
        private SimpleDateFormat simpleDateFormat;

        private Builder() {
        }

        public Builder withTransactionIdProperties(TransactionIdProperties transactionIdProperties) {
            this.transactionIdProperties = transactionIdProperties;
            return this;
        }

        public Builder withSimpleDateFormat(SimpleDateFormat simpleDateFormat) {
            this.simpleDateFormat = simpleDateFormat;
            return this;
        }

        public TransactionIdGeneratingServiceImpl build() {
            return new TransactionIdGeneratingServiceImpl(this);
        }
    }
}
