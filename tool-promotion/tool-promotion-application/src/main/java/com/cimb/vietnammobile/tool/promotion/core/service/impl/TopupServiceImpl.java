package com.cimb.vietnammobile.tool.promotion.core.service.impl;

import com.cimb.vietnammobile.tool.promotion.core.configuration.Constants;
import com.cimb.vietnammobile.tool.promotion.core.model.TopupRequest;
import com.cimb.vietnammobile.tool.promotion.core.model.TopupResult;
import com.cimb.vietnammobile.tool.promotion.core.service.AccessTokenService;
import com.cimb.vietnammobile.tool.promotion.core.service.TopupService;
import com.cimb.vietnammobile.tool.promotion.core.service.exception.TopupCommunicatingException;
import com.cimb.vietnammobile.tool.promotion.core.service.properties.TopupProperties;
import com.cimb.vietnammobile.tool.promotion.infrastructure.rest.model.request.TopupRestfulRequest;
import com.cimb.vietnammobile.tool.promotion.infrastructure.rest.model.response.TopupRestfulResponse;
import com.cimb.vietnammobile.tools.util.AbbreviationUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class TopupServiceImpl implements TopupService {
    private static final Logger log = LoggerFactory.getLogger(TopupServiceImpl.class);

    private static final String REMARK_TEMPLATE = "Please check: %s";
    private static final String REMARK_UNKNOWN_RESULT_CODE = "Unknown result code";
    private static final String REMARK_STRANGE_VALUE_IN_RESTFUL_RESPONSE =
            "Topup response contains strange values for phone number (%s) and transaction id (%s)";
    private static final String REMARK_DELIMITER = ",";
    private static final String RESULT_MESSAGE_COMMUNICATION_ERROR = "Communication Error";
    private static final String LOG_TEMPLATE_PROCESS_TOPUP_REQUEST = "Process topupRequest: %s";
    private static final String LOG_TEMPLATE_RECEIVE_TOPUP_RESULT = "Receive topupResult: %s";
    private static final String LOG_TEMPLATE_RECEIVE_ERROR_WHILE_PROCESSING_REQUEST = "Receive error while processing request: %s";
    private static final String LOG_TEMPLATE_TRANSLATE_ERROR_TO_TOPUP_RESULT = "Translate-error-to-topup-result: %s";

    private AccessTokenService accessTokenService;
    private TopupProperties topupProperties;
    private RestTemplate topupRestTemplate;
    private URI topupUri;

    private TopupServiceImpl(Builder builder) {
        accessTokenService = builder.accessTokenService;
        topupProperties = builder.topupProperties;
        topupRestTemplate = builder.topupRestTemplate;
        topupUri = builder.topupUri;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    @Override
    public TopupResult topup(TopupRequest request) {
        log.debug(String.format(LOG_TEMPLATE_PROCESS_TOPUP_REQUEST, request));
        TopupResult topupResult;
        try {
            topupResult = topupImpl(request);
            log.debug(String.format(LOG_TEMPLATE_RECEIVE_TOPUP_RESULT, topupResult));
        } catch (Throwable throwable) {
            log.debug(String.format(LOG_TEMPLATE_RECEIVE_ERROR_WHILE_PROCESSING_REQUEST, request), throwable);
            topupResult = this.translateErrorToTopupResult(request, throwable);
            log.debug(String.format(LOG_TEMPLATE_TRANSLATE_ERROR_TO_TOPUP_RESULT, topupResult));
        }
        return topupResult;
    }

    private TopupResult topupImpl(TopupRequest request) {
        String accessToken = accessTokenService.getAccessToken();
        TopupRestfulRequest restfulRequest = this.createRestfulRequest(request, accessToken);
        ResponseEntity<TopupRestfulResponse> restfulResponse = this.postForRestfulResponse(restfulRequest);

        return this.extractTopupResult(restfulResponse, restfulRequest);
    }

    private TopupResult translateErrorToTopupResult(TopupRequest request, Throwable throwable) {
        return TopupResult
                .newBuilder()
                .withPhoneNumber(request.getPhoneNumber())
                .withTransactionId(request.getTransactionId())
                .withResultCode(Constants.RESULT_CODE_COMMUNICATION_ERROR)
                .withResultMessage(RESULT_MESSAGE_COMMUNICATION_ERROR)
                .withRemark(this.remarkOf(throwable))
                .build();
    }

    private TopupRestfulRequest createRestfulRequest(TopupRequest request, String accessToken) {
        return TopupRestfulRequest
                .newBuilder()
                .withIsdn(request.getPhoneNumber())
                .withToken(accessToken)
                .withAppid(topupProperties.getAppid())
                .withTransactionid(request.getTransactionId())
                .build();
    }

    private ResponseEntity<TopupRestfulResponse> postForRestfulResponse(TopupRestfulRequest restfulRequest) {
        try {
            return topupRestTemplate.postForEntity(topupUri, restfulRequest, TopupRestfulResponse.class);
        } catch (HttpStatusCodeException ex) {
            throw new TopupCommunicatingException(restfulRequest, ex);
        } catch (Throwable throwable) {
            throw new TopupCommunicatingException(restfulRequest, throwable);
        }
    }

    private TopupResult extractTopupResult(ResponseEntity<TopupRestfulResponse> restfulResponse, TopupRestfulRequest restfulRequest) {
        return TopupResult
                .newBuilder()
                .withPhoneNumber(restfulRequest.getIsdn())
                .withTransactionId(restfulRequest.getTransactionid())
                .withResultCode(this.resultCodeOf(restfulResponse))
                .withResultMessage(this.resultMessageOf(restfulResponse))
                .withRemark(this.remarkOf(restfulResponse, restfulRequest))
                .build();
    }

    private Integer resultCodeOf(ResponseEntity<TopupRestfulResponse> restfulResponse) {
        return restfulResponse.getBody().getResultcode();
    }

    private String resultMessageOf(ResponseEntity<TopupRestfulResponse> restfulResponse) {
        return restfulResponse.getBody().getMessage();
    }

    private String remarkOf(ResponseEntity<TopupRestfulResponse> restfulResponse, TopupRestfulRequest restfulRequest) {
        List<String> remarks = new ArrayList<>();

        if (!isExpectedResultCode(restfulResponse.getBody().getResultcode(), restfulResponse.getStatusCode())) {
            remarks.add(REMARK_UNKNOWN_RESULT_CODE);
        }
        if (!isExpectedResponseData(restfulResponse.getBody(), restfulRequest)) {
            remarks.add(String.format(REMARK_STRANGE_VALUE_IN_RESTFUL_RESPONSE,
                    restfulResponse.getBody().getIsdn(),
                    restfulResponse.getBody().getTransactionid()));
        }

        String remark = remarks.stream().collect(Collectors.joining(REMARK_DELIMITER));

        return StringUtils.isNotBlank(remark)
                ? String.format(REMARK_TEMPLATE, remark)
                : StringUtils.EMPTY;
    }

    private String remarkOf(Throwable throwable) {
        return AbbreviationUtils.of(throwable);
    }

    private Boolean isExpectedResultCode(Integer resultCode, HttpStatus httpStatus) {
        return Objects.equals(httpStatus, HttpStatus.OK)
                && (Objects.equals(resultCode, Constants.RESULT_CODE_SUCCESS)
                || Objects.equals(resultCode, Constants.RESULT_CODE_INVALID_TOKEN)
                || Objects.equals(resultCode, Constants.RESULT_CODE_TRANSACTION_IN_PROCESS));
    }

    private Boolean isExpectedResponseData(TopupRestfulResponse restfulResponse, TopupRestfulRequest restfulRequest) {
        return Objects.equals(restfulResponse.getIsdn(), restfulRequest.getIsdn())
                && Objects.equals(restfulResponse.getTransactionid(), restfulRequest.getTransactionid());
    }

    public static final class Builder {
        private AccessTokenService accessTokenService;
        private RestTemplate topupRestTemplate;
        private TopupProperties topupProperties;
        private URI topupUri;

        private Builder() {
        }

        public Builder withAccessTokenService(AccessTokenService accessTokenService) {
            this.accessTokenService = accessTokenService;
            return this;
        }

        public Builder withTopupProperties(TopupProperties topupProperties) {
            this.topupProperties = topupProperties;
            return this;
        }

        public Builder withTopupRestTemplate(RestTemplate topupRestTemplate) {
            this.topupRestTemplate = topupRestTemplate;
            return this;
        }

        public Builder withTopupUri(URI topupUri) {
            this.topupUri = topupUri;
            return this;
        }

        public TopupServiceImpl build() {
            return new TopupServiceImpl(this);
        }
    }
}
