package com.cimb.vietnammobile.tool.promotion.core.service.helper;

import com.cimb.vietnammobile.tool.promotion.core.configuration.Constants;
import com.cimb.vietnammobile.tool.promotion.core.service.exception.KeyhashComputingException;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;

public class KeyhashHelper {
    private String keyhash;

    private KeyhashHelper(Builder builder) {
        keyhash = this.createKeyhash(builder.password, builder.aesKey);
    }

    final String createKeyhash(String password, String aesKey) {
        try {
            SecretKey secretKey = new SecretKeySpec(Base64.getDecoder().decode(aesKey), Constants.KEYHASH_ENCRYPTION_ALGORITHM);
            Cipher cipher = Cipher.getInstance(Constants.KEYHASH_ENCRYPTION_ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            byte[] encryptedBytes = cipher.doFinal(password.getBytes());
            return Base64.getEncoder().encodeToString(encryptedBytes);
        } catch (Throwable throwable) {
            throw new KeyhashComputingException(password, aesKey, throwable);
        }
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getKeyhash() {
        return keyhash;
    }

    public static final class Builder {
        private String password;
        private String aesKey;

        private Builder() {
        }

        public Builder withPassword(String password) {
            this.password = password;
            return this;
        }

        public Builder withAesKey(String aesKey) {
            this.aesKey = aesKey;
            return this;
        }

        public KeyhashHelper build() {
            return new KeyhashHelper(this);
        }
    }
}
