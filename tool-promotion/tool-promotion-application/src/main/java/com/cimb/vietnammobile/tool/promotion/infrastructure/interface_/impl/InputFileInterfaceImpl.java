package com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.impl;

import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.InputFileInterface;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.exception.InputFileAccessingException;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.properties.InputFileInterfaceProperties;
import com.cimb.vietnammobile.tools.util.FileUtils;

import java.nio.file.Path;
import java.util.Optional;

public class InputFileInterfaceImpl implements InputFileInterface {
    private InputFileInterfaceProperties inputFileInterfaceProperties;

    private InputFileInterfaceImpl(Builder builder) {
        inputFileInterfaceProperties = builder.inputFileInterfaceProperties;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    @Override
    public Path inputFile() {
        try {
            return Optional
                    .ofNullable(
                            FileUtils.existingReadableFilePathOf(inputFileInterfaceProperties.getFileName()))
                    .orElseThrow(NullPointerException::new);
        } catch (Throwable throwable) {
            throw new InputFileAccessingException(inputFileInterfaceProperties.getFileName(), throwable);
        }
    }

    public static final class Builder {
        private InputFileInterfaceProperties inputFileInterfaceProperties;

        private Builder() {
        }

        public Builder withInputFileInterfaceProperties(InputFileInterfaceProperties inputFileInterfaceProperties) {
            this.inputFileInterfaceProperties = inputFileInterfaceProperties;
            return this;
        }

        public InputFileInterfaceImpl build() {
            return new InputFileInterfaceImpl(this);
        }
    }
}
