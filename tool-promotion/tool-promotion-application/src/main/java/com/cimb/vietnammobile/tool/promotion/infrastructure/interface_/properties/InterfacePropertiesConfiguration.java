package com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.properties;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(value = {
        InputFileInterfaceProperties.class,
        OutputFileInterfaceProperties.class,
        CommandInterfaceProperties.class
})
public class InterfacePropertiesConfiguration {
}
