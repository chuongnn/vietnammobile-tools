package com.cimb.vietnammobile.tool.promotion.core.service.helper;

import com.cimb.vietnammobile.tool.promotion.core.model.TopupResult;
import com.univocity.parsers.csv.CsvWriter;

import javax.annotation.PreDestroy;

public class TopupResultWritingHelper {
    private CsvWriter csvWriter;

    private TopupResultWritingHelper(Builder builder) {
        csvWriter = builder.csvWriter;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public void write(TopupResult result) {
        csvWriter.writeRow(
                result.getPhoneNumber(),
                result.getTransactionId(),
                result.getResultCode(),
                result.getResultMessage(),
                result.getRemark()
        );
        csvWriter.flush();
    }

    @PreDestroy
    public void shutdown() {
        csvWriter.flush();
        csvWriter.close();
    }

    public static final class Builder {
        private CsvWriter csvWriter;

        private Builder() {
        }

        public Builder withCsvWriter(CsvWriter csvWriter) {
            this.csvWriter = csvWriter;
            return this;
        }

        public TopupResultWritingHelper build() {
            return new TopupResultWritingHelper(this);
        }
    }
}
