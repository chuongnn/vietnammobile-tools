package com.cimb.vietnammobile.tool.promotion.core.service.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("vietnammobile.core.transaction-id")
public class TransactionIdProperties {
    private String prefix;
    private String pattern;

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }
}
