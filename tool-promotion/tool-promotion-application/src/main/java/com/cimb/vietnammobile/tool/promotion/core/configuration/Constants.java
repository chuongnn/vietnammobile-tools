package com.cimb.vietnammobile.tool.promotion.core.configuration;

public class Constants {
    public static final Integer RESULT_CODE_COMMUNICATION_ERROR = -1;
    public static final Integer RESULT_CODE_SUCCESS = 0;
    public static final Integer RESULT_CODE_INVALID_TOKEN = 4;
    public static final Integer RESULT_CODE_TRANSACTION_IN_PROCESS = 6;
    public static final String KEYHASH_ENCRYPTION_ALGORITHM = "AES";
    public static final String DEFAULT_TIMEZONE_OFFSET = "+07:00";
}
