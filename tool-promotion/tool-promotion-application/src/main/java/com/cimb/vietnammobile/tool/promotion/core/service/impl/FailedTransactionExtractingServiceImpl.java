package com.cimb.vietnammobile.tool.promotion.core.service.impl;

import com.cimb.vietnammobile.tool.promotion.core.model.TopupRequest;
import com.cimb.vietnammobile.tool.promotion.core.service.FailedTransactionExtractingService;
import com.cimb.vietnammobile.tool.promotion.core.service.exception.FailedTransactionExtractingException;
import com.cimb.vietnammobile.tool.promotion.core.service.helper.FailedTransactionExtractingHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;
import java.util.List;

public class FailedTransactionExtractingServiceImpl implements FailedTransactionExtractingService {
    private static final Logger log = LoggerFactory.getLogger(FailedTransactionExtractingServiceImpl.class);
    private static final String LOG_TEMPLATE_EXTRACT_FAILED_TRANSACTIONS_FROM_INPUT_FILE =
            "Extract failed transactions from inputFile=%s";

    private FailedTransactionExtractingHelper failedTransactionExtractingHelper;

    private FailedTransactionExtractingServiceImpl(Builder builder) {
        failedTransactionExtractingHelper = builder.failedTransactionExtractingHelper;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    @Override
    public List<TopupRequest> extract(Path inputFile) {
        try {
            log.info(String.format(LOG_TEMPLATE_EXTRACT_FAILED_TRANSACTIONS_FROM_INPUT_FILE, inputFile));
            return failedTransactionExtractingHelper.extract(inputFile);
        } catch (Throwable throwable) {
            throw new FailedTransactionExtractingException(inputFile, throwable);
        }
    }

    public static final class Builder {
        private FailedTransactionExtractingHelper failedTransactionExtractingHelper;

        private Builder() {
        }

        public Builder withFailedTransactionExtractingHelper(FailedTransactionExtractingHelper failedTransactionExtractingHelper) {
            this.failedTransactionExtractingHelper = failedTransactionExtractingHelper;
            return this;
        }

        public FailedTransactionExtractingServiceImpl build() {
            return new FailedTransactionExtractingServiceImpl(this);
        }
    }
}
