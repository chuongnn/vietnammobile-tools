package com.cimb.vietnammobile.tool.promotion.core.service.properties;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(value = {
        LoginProperties.class,
        TopupProperties.class,
        TransactionIdProperties.class
})
public class ServicePropertiesConfiguration {
}
