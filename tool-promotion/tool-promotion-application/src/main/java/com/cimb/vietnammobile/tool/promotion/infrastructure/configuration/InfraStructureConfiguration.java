package com.cimb.vietnammobile.tool.promotion.infrastructure.configuration;

import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.configuration.InterfaceConfiguration;
import com.cimb.vietnammobile.tool.promotion.infrastructure.interface_.properties.InterfacePropertiesConfiguration;
import com.cimb.vietnammobile.tool.promotion.infrastructure.rest.configuration.RestClientConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(value = {
        InterfacePropertiesConfiguration.class,
        InterfaceConfiguration.class,
        RestClientConfiguration.class
})
@EnableAutoConfiguration
public class InfraStructureConfiguration {
}
