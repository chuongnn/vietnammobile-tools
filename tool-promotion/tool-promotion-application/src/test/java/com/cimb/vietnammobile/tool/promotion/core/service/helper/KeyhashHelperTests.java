package com.cimb.vietnammobile.tool.promotion.core.service.helper;

import com.cimb.vietnammobile.tool.promotion.core.TestData;
import com.cimb.vietnammobile.tool.promotion.core.service.exception.KeyhashComputingException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class KeyhashHelperTests {
    private KeyhashHelper helper = KeyhashHelper
            .newBuilder()
            .withPassword(TestData.SAMPLE_PASSWORD)
            .withAesKey(TestData.SAMPLE_AES_KEY)
            .build();

    @Test
    public void createKeyhash_shouldProduceAKeyHash_afterProcessingValidInputs() {
        //GIVEN: a valid password and a valid aes 128-bit key
        String password = TestData.SAMPLE_PASSWORD;
        String aesKey = TestData.SAMPLE_AES_KEY;

        //WHEN: call createKeyhash with given password and aseKey
        String keyhash = helper.createKeyhash(password, aesKey);

        //THEN: a non-null keyhash is returned
        Assert.assertNotNull(keyhash);
    }

    @Test(expected = KeyhashComputingException.class)
    public void createKeyhash_shouldThrowException_afterProcessingNullPassword() {
        //GIVEN: a null password and a valid aes 128-bit key
        String password = null;
        String aesKey = TestData.SAMPLE_AES_KEY;

        //WHEN: call createKeyhash with given password and aseKey
        helper.createKeyhash(password, aesKey);

        //THEN: an exception is thrown
    }

    @Test(expected = KeyhashComputingException.class)
    public void createKeyhash_shouldThrowException_afterProcessingInvalidAesKey() {
        //GIVEN: a null password and a valid aes 128-bit key
        String password = TestData.SAMPLE_PASSWORD;
        String aesKey = TestData.SAMPLE_AES_KEY.substring(1);

        //WHEN: call createKeyhash with given password and aseKey
        helper.createKeyhash(password, aesKey);

        //THEN: an exception is thrown
    }
}