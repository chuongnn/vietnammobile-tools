package com.cimb.vietnammobile.tool.promotion.mocked.api.statemachine;

import org.springframework.stereotype.Component;

@Component
public class StateMachine {
    private State state = State.NORMAL;

    public void setState(State state) {
        this.state = state;
    }

    public State getState() {
        return state;
    }
}
