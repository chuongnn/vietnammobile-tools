package com.cimb.vietnammobile.tool.promotion.mocked.api.helper.exception;

public class KeyHashDecryptingException extends RuntimeException{
    private static final String MESSAGE_TEMPLATE = "Error while decrypting keyhash=%s using aesKey=%s";

    public KeyHashDecryptingException(String keyhash, String aesKey, Throwable throwable) {
        super(
                String.format(MESSAGE_TEMPLATE, keyhash, aesKey),
                throwable
        );
    }
}
