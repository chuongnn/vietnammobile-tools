package com.cimb.vietnammobile.tool.promotion.mocked.api.helper.configuration;

import com.cimb.vietnammobile.tool.promotion.mocked.api.configuration.properties.LoginProperties;
import com.cimb.vietnammobile.tool.promotion.mocked.api.helper.KeyHashHelper;
import com.cimb.vietnammobile.tool.promotion.mocked.api.helper.LoginHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(value = {
        LoginProperties.class
})
public class HelperConfiguration {
    @Autowired
    private LoginProperties loginProperties;

    @Bean
    public LoginHelper loginHelper() {
        return LoginHelper
                .newBuilder()
                .withLoginProperties(loginProperties)
                .withDecryptingHelper(new KeyHashHelper())
                .build();
    }
}
