package com.cimb.vietnammobile.tool.promotion.mocked.api.controller;

import com.cimb.vietnammobile.tool.promotion.mocked.api.configuration.Constants;
import com.cimb.vietnammobile.tool.promotion.mocked.api.model.request.TopupRestfulRequest;
import com.cimb.vietnammobile.tool.promotion.mocked.api.model.response.TopupRestfulResponse;
import com.cimb.vietnammobile.tool.promotion.mocked.api.statemachine.State;
import com.cimb.vietnammobile.tool.promotion.mocked.api.statemachine.StateMachine;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

@RestController
@RequestMapping(Constants.URI_TOPUP)
public class TopupController {
    @Autowired
    private StateMachine stateMachine;

    private List<Pair<
                    Function<TopupRestfulRequest, Boolean>,
                    Function<TopupRestfulRequest, ResponseEntity<TopupRestfulResponse>>
            >> decisionTable;

    @PostConstruct
    public void init() {
        initDecisionTable();
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<TopupRestfulResponse> topup(@RequestBody TopupRestfulRequest request) {
        return this.decisionTable
                .stream()
                .filter(_pair -> _pair.getLeft().apply(request))
                .findFirst()
                .map(_pair -> _pair.getRight().apply(request))
                .orElse(null);
    }

    private void initDecisionTable() {
        decisionTable = new ArrayList<>();
        decisionTable.add(Pair.of(this::shouldResponseNormally, this::responseNormally));
        decisionTable.add(Pair.of(this::shouldResponseUnknownHttpStatus, this::responseUnknownHttpStatus));
        decisionTable.add(Pair.of(this::shouldResponseEmptyInternalServerError, this::responseEmptyInternalServerError));
        decisionTable.add(Pair.of(this::shouldResponseUnknownResultCodeInInternalServerError, this::responseUnknownResultCodeInInternalServerError));
        decisionTable.add(Pair.of(this::shouldResponseUnknownResultCodeInOk, this::responseUnknownResultCodeInOk));
        decisionTable.add(Pair.of(this::shouldResponseStrangeValuesInOk, this::responseStrangeValuesInOk));
        decisionTable.add(Pair.of(this::shouldResponseTransactionInProcess, this::responseTransactionInProcess));
        decisionTable.add(Pair.of(this::shouldResponseInvalidToken, this::responseInvalidToken));
    }

    private Boolean shouldResponseNormally(TopupRestfulRequest restfulRequest) {
        return !(shouldResponseUnknownHttpStatus(restfulRequest)
                || shouldResponseEmptyInternalServerError(restfulRequest)
                || shouldResponseUnknownResultCodeInInternalServerError(restfulRequest)
                || shouldResponseUnknownResultCodeInOk(restfulRequest)
                || shouldResponseStrangeValuesInOk(restfulRequest)
                || shouldResponseTransactionInProcess(restfulRequest)
                || shouldResponseInvalidToken(restfulRequest)
                );
    }

    private Boolean shouldResponseUnknownHttpStatus(TopupRestfulRequest restfulRequest) {
        return Objects.equals(stateMachine.getState(), State.TOPUP_RETURN_UNKNOWN_HTTP_STATUS);
    }

    private Boolean shouldResponseEmptyInternalServerError(TopupRestfulRequest restfulRequest) {
        return Objects.equals(stateMachine.getState(), State.TOPUP_RETURN_EMPTY_INTERNAL_SERVER_ERROR);
    }

    private Boolean shouldResponseUnknownResultCodeInInternalServerError(TopupRestfulRequest restfulRequest) {
        return Objects.equals(stateMachine.getState(), State.TOPUP_RETURN_UNKNOWN_RESULT_CODE_IN_INTERNAL_SERVER_ERROR);
    }

    private Boolean shouldResponseUnknownResultCodeInOk(TopupRestfulRequest restfulRequest) {
        return Objects.equals(stateMachine.getState(), State.TOPUP_RETURN_UNKNOWN_RESULT_CODE_IN_OK);
    }

    private Boolean shouldResponseStrangeValuesInOk(TopupRestfulRequest restfulRequest) {
        return Objects.equals(stateMachine.getState(), State.TOPUP_RETURN_STRANGE_VALUES_IN_OK);
    }

    private Boolean shouldResponseTransactionInProcess(TopupRestfulRequest restfulRequest) {
        return Objects.equals(stateMachine.getState(), State.TOPUP_RETURN_TRANSACTION_IN_PROCESS);
    }

    private Boolean shouldResponseInvalidToken(TopupRestfulRequest restfulRequest) {
        return Objects.equals(stateMachine.getState(), State.TOPUP_RETURN_INVALID_TOKEN);
    }

    private ResponseEntity<TopupRestfulResponse> responseNormally(TopupRestfulRequest restfulRequest) {
        if (Objects.equals(restfulRequest.getToken(), Constants.ACCESS_TOKEN)) {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(TopupRestfulResponse
                            .newBuilder()
                            .withIsdn(restfulRequest.getIsdn())
                            .withTransactionid(restfulRequest.getTransactionid())
                            .withResultcode(Constants.RESULT_CODE_SUCCESS)
                            .withMessage(Constants.RESULT_MESSAGE_SUCCESS)
                            .build());
        } else {
            return ResponseEntity
                    .status(HttpStatus.OK)
                    .body(TopupRestfulResponse
                            .newBuilder()
                            .withIsdn(restfulRequest.getIsdn())
                            .withTransactionid(restfulRequest.getTransactionid())
                            .withResultcode(Constants.RESULT_CODE_INVALID_TOKEN)
                            .withMessage(Constants.RESULT_MESSAGE_INVALID_TOKEN)
                            .build());
        }
    }

    private ResponseEntity<TopupRestfulResponse> responseUnknownHttpStatus(TopupRestfulRequest restfulRequest) {
        return ResponseEntity
                .status(HttpStatus.BAD_REQUEST)
                .build();
    }

    private ResponseEntity<TopupRestfulResponse> responseEmptyInternalServerError(TopupRestfulRequest restfulRequest) {
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .build();
    }

    private ResponseEntity<TopupRestfulResponse> responseUnknownResultCodeInInternalServerError(TopupRestfulRequest restfulRequest) {
        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(TopupRestfulResponse
                        .newBuilder()
                        .withIsdn(restfulRequest.getIsdn())
                        .withTransactionid(restfulRequest.getTransactionid())
                        .withResultcode(Constants.RESULT_CODE_UNKNOWN)
                        .withMessage(Constants.RESULT_MESSAGE_UNKNOWN)
                        .build());
    }

    private ResponseEntity<TopupRestfulResponse> responseUnknownResultCodeInOk(TopupRestfulRequest restfulRequest) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(TopupRestfulResponse
                        .newBuilder()
                        .withIsdn(restfulRequest.getIsdn())
                        .withTransactionid(restfulRequest.getTransactionid())
                        .withResultcode(Constants.RESULT_CODE_UNKNOWN)
                        .withMessage(Constants.RESULT_MESSAGE_UNKNOWN)
                        .build());
    }

    private ResponseEntity<TopupRestfulResponse> responseStrangeValuesInOk(TopupRestfulRequest restfulRequest) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(TopupRestfulResponse
                        .newBuilder()
                        .withIsdn(restfulRequest.getIsdn() + ".hacked")
                        .withTransactionid(restfulRequest.getTransactionid() + ".hacked")
                        .withResultcode(Constants.RESULT_CODE_SUCCESS)
                        .withMessage(Constants.RESULT_MESSAGE_SUCCESS)
                        .build());
    }

    private ResponseEntity<TopupRestfulResponse> responseTransactionInProcess(TopupRestfulRequest restfulRequest) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(TopupRestfulResponse
                        .newBuilder()
                        .withIsdn(restfulRequest.getIsdn())
                        .withTransactionid(restfulRequest.getTransactionid())
                        .withResultcode(Constants.RESULT_CODE_TRANSACTION_IN_PROCESS)
                        .withMessage(Constants.RESULT_MESSAGE_TRANSACTION_IN_PROCESS)
                        .build());
    }

    private ResponseEntity<TopupRestfulResponse> responseInvalidToken(TopupRestfulRequest restfulRequest) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(TopupRestfulResponse
                        .newBuilder()
                        .withIsdn(restfulRequest.getIsdn())
                        .withTransactionid(restfulRequest.getTransactionid())
                        .withResultcode(Constants.RESULT_CODE_INVALID_TOKEN)
                        .withMessage(Constants.RESULT_MESSAGE_INVALID_TOKEN)
                        .build());
    }
}
