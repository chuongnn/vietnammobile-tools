package com.cimb.vietnammobile.tool.promotion.mocked.api.model.request;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class AccessTokenRestfulRequest {
    private String username;
    private String keyhash;
    private String appid;

    public AccessTokenRestfulRequest() {
    }

    private AccessTokenRestfulRequest(Builder builder) {
        username = builder.username;
        keyhash = builder.keyhash;
        appid = builder.appid;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getUsername() {
        return username;
    }

    public String getKeyhash() {
        return keyhash;
    }

    public String getAppid() {
        return appid;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)
                .append("username", username)
                .append("keyhash", keyhash)
                .append("appid", appid)
                .toString();
    }

    public static final class Builder {
        private String username;
        private String keyhash;
        private String appid;

        private Builder() {
        }

        public Builder withUsername(String username) {
            this.username = username;
            return this;
        }

        public Builder withKeyhash(String keyhash) {
            this.keyhash = keyhash;
            return this;
        }

        public Builder withAppid(String appid) {
            this.appid = appid;
            return this;
        }

        public AccessTokenRestfulRequest build() {
            return new AccessTokenRestfulRequest(this);
        }
    }
}
