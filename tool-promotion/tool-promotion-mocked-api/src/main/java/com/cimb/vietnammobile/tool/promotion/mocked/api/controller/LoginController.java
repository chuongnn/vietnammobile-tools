package com.cimb.vietnammobile.tool.promotion.mocked.api.controller;

import com.cimb.vietnammobile.tool.promotion.mocked.api.configuration.Constants;
import com.cimb.vietnammobile.tool.promotion.mocked.api.helper.exception.KeyHashDecryptingException;
import com.cimb.vietnammobile.tool.promotion.mocked.api.helper.LoginHelper;
import com.cimb.vietnammobile.tool.promotion.mocked.api.model.request.AccessTokenRestfulRequest;
import com.cimb.vietnammobile.tool.promotion.mocked.api.model.response.AccessTokenRestfulResponse;
import com.cimb.vietnammobile.tool.promotion.mocked.api.statemachine.State;
import com.cimb.vietnammobile.tool.promotion.mocked.api.statemachine.StateMachine;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

@RestController
@RequestMapping(Constants.URI_LOGIN)
public class LoginController {
    @Autowired
    private StateMachine stateMachine;
    @Autowired
    private LoginHelper loginHelper;

    private List<Pair<
                Function<AccessTokenRestfulRequest, Boolean>,
                Function<AccessTokenRestfulRequest, ResponseEntity<AccessTokenRestfulResponse>>
            >> decisionTable;

    @PostConstruct
    public void init() {
        initDecisionTable();
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<AccessTokenRestfulResponse> login(@RequestBody AccessTokenRestfulRequest request) {
        return this.decisionTable
                .stream()
                .filter(_pair -> _pair.getLeft().apply(request))
                .findFirst()
                .map(_pair -> _pair.getRight().apply(request))
                .orElse(null);
    }

    private void initDecisionTable() {
        decisionTable = new ArrayList<>();
        decisionTable.add(Pair.of(this::shouldResponseNormally, this::responseNormally));
        decisionTable.add(Pair.of(this::shouldResponseUnknownHttpStatus, this::responseUnknownHttpStatus));
        decisionTable.add(Pair.of(this::shouldResponseEmptyInternalServerError, this::responseEmptyInternalServerError));
    }

    private Boolean shouldResponseNormally(AccessTokenRestfulRequest request) {
        return !(shouldResponseUnknownHttpStatus(request)
                || shouldResponseEmptyInternalServerError(request));
    }

    private Boolean shouldResponseUnknownHttpStatus(AccessTokenRestfulRequest request) {
        return Objects.equals(stateMachine.getState(), State.LOGIN_RETURN_UNKNOWN_HTTP_STATUS);
    }

    private Boolean shouldResponseEmptyInternalServerError(AccessTokenRestfulRequest request) {
        return Objects.equals(stateMachine.getState(), State.LOGIN_RETURNED_EMPTY_INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity<AccessTokenRestfulResponse> responseNormally(AccessTokenRestfulRequest request) {
        try {
            Boolean shouldAuthorize = loginHelper.shouldAuthorize(
                    request.getUsername(),
                    request.getKeyhash(),
                    request.getAppid());
            if (shouldAuthorize) {
                return ResponseEntity
                        .status(HttpStatus.OK)
                        .body(AccessTokenRestfulResponse
                                .newBuilder()
                                .withResultcode(Constants.RESULT_CODE_SUCCESS)
                                .withMessage(Constants.ACCESS_TOKEN)
                                .build()
                        );
            } else {
                return ResponseEntity
                        .status(HttpStatus.UNAUTHORIZED)
                        .body(AccessTokenRestfulResponse
                                .newBuilder()
                                .withResultcode(Constants.RESULT_CODE_LOGIN_FAILED)
                                .withMessage(Constants.RESULT_MESSAGE_LOGIN_FAILED)
                                .build()
                        );
            }
        } catch (KeyHashDecryptingException exception) {
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(AccessTokenRestfulResponse
                            .newBuilder()
                            .withResultcode(Constants.RESULT_CODE_CANNOT_DECRYPT_MESSAGE)
                            .withMessage(Constants.RESULT_MESSAGE_CANNOT_DECRYPT_MESSAGE)
                            .build()
                    );
        }
    }

    private ResponseEntity<AccessTokenRestfulResponse> responseUnknownHttpStatus(AccessTokenRestfulRequest request) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    private ResponseEntity<AccessTokenRestfulResponse> responseEmptyInternalServerError(AccessTokenRestfulRequest request) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
    }
}
