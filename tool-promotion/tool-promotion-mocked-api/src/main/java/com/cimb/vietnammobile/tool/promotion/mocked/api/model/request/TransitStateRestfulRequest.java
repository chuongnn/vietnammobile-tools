package com.cimb.vietnammobile.tool.promotion.mocked.api.model.request;

import com.cimb.vietnammobile.tool.promotion.mocked.api.statemachine.State;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class TransitStateRestfulRequest {
    private State state;

    public TransitStateRestfulRequest() {
    }

    private TransitStateRestfulRequest(Builder builder) {
        state = builder.state;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public State getState() {
        return state;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)
                .append("state", state)
                .toString();
    }

    public static final class Builder {
        private State state;

        private Builder() {
        }

        public Builder withState(State state) {
            this.state = state;
            return this;
        }

        public TransitStateRestfulRequest build() {
            return new TransitStateRestfulRequest(this);
        }
    }
}
