package com.cimb.vietnammobile.tool.promotion.mocked.api.controller;

import com.cimb.vietnammobile.tool.promotion.mocked.api.configuration.Constants;
import com.cimb.vietnammobile.tool.promotion.mocked.api.model.request.TransitStateRestfulRequest;
import com.cimb.vietnammobile.tool.promotion.mocked.api.statemachine.StateMachine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(Constants.URI_STATEMACHINE)
public class StateMachineController {
    @Autowired
    private StateMachine stateMachine;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity transitState(@RequestBody TransitStateRestfulRequest request) {
        stateMachine.setState(request.getState());
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
