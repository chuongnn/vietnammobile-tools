package com.cimb.vietnammobile.tool.promotion.mocked.api.helper;

import com.cimb.vietnammobile.tool.promotion.mocked.api.configuration.Constants;
import com.cimb.vietnammobile.tool.promotion.mocked.api.helper.exception.KeyHashDecryptingException;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;

public class KeyHashHelper {
    public String decrypt(String keyhash, String aesKey) {
        try {
            SecretKey secretKey = new SecretKeySpec(Base64.getDecoder().decode(aesKey), Constants.KEYHASH_ENCRYPTION_ALGORITHM);
            Cipher cipher = Cipher.getInstance(Constants.KEYHASH_ENCRYPTION_ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            byte[] decryptedBytes = cipher.doFinal(Base64.getDecoder().decode(keyhash));
            return new String(decryptedBytes);
        } catch (Throwable throwable) {
            throw new KeyHashDecryptingException(keyhash, aesKey, throwable);
        }
    }
}
