package com.cimb.vietnammobile.tool.promotion.mocked.api.helper;

import com.cimb.vietnammobile.tool.promotion.mocked.api.configuration.properties.LoginProperties;
import org.apache.commons.lang3.StringUtils;

public class LoginHelper {
    private LoginProperties loginProperties;
    private KeyHashHelper keyHashHelper;

    private LoginHelper(Builder builder) {
        loginProperties = builder.loginProperties;
        keyHashHelper = builder.keyHashHelper;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public Boolean shouldAuthorize(String username, String keyhash, String appid) {
        String password = keyHashHelper.decrypt(keyhash, loginProperties.getAesKey());

        return StringUtils.equals(loginProperties.getUsername(), username)
                && StringUtils.equals(loginProperties.getAppid(), appid)
                && StringUtils.equals(loginProperties.getPassword(), password);
    }

    public static final class Builder {
        private LoginProperties loginProperties;
        private KeyHashHelper keyHashHelper;

        private Builder() {
        }

        public Builder withLoginProperties(LoginProperties loginProperties) {
            this.loginProperties = loginProperties;
            return this;
        }

        public Builder withDecryptingHelper(KeyHashHelper keyHashHelper) {
            this.keyHashHelper = keyHashHelper;
            return this;
        }

        public LoginHelper build() {
            return new LoginHelper(this);
        }
    }
}
