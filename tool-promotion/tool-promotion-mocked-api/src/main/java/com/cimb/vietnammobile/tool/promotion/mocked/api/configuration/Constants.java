package com.cimb.vietnammobile.tool.promotion.mocked.api.configuration;

public class Constants {
    private static final String URI_API = "topup";
    public static final String URI_LOGIN = URI_API + "/login";
    public static final String URI_TOPUP = URI_API + "/gettopup";
    public static final String URI_STATEMACHINE = URI_API + "/statemachine";
    public static final String ACCESS_TOKEN = "eyJhbGciOiJIUzI1NiJ9." +
            "eyJzdWIiOiJGUFRfQU5ITFQjIzE1Mjg5LS0tW0JANzE4MDk5MDciLCJleHAiOjE1MTU2NDg4OTF9." +
            "fTNbNUeqYup_MP00IkEQwZP-2kILTq7MfPWLbz9Xmzs";
    public static final Integer RESULT_CODE_SUCCESS = 0;
    public static final Integer RESULT_CODE_LOGIN_FAILED = 2;
    public static final Integer RESULT_CODE_CANNOT_DECRYPT_MESSAGE = 3;
    public static final Integer RESULT_CODE_INVALID_TOKEN = 4;
    public static final Integer RESULT_CODE_TRANSACTION_IN_PROCESS = 6;
    public static final Integer RESULT_CODE_UNKNOWN = 100;
    public static final String RESULT_MESSAGE_SUCCESS = "success";
    public static final String RESULT_MESSAGE_LOGIN_FAILED = "Login Failed";
    public static final String RESULT_MESSAGE_CANNOT_DECRYPT_MESSAGE = "Cannot Decrypt Message";
    public static final String RESULT_MESSAGE_INVALID_TOKEN = "Invalid Token";
    public static final String RESULT_MESSAGE_TRANSACTION_IN_PROCESS = "Transaction In Process";
    public static final String RESULT_MESSAGE_UNKNOWN = "Unknown Result Code";
    public static final String KEYHASH_ENCRYPTION_ALGORITHM = "AES";
}
