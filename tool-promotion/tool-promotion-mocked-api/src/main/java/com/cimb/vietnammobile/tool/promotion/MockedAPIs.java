package com.cimb.vietnammobile.tool.promotion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MockedAPIs {
    public static void main(String[] args) {
        SpringApplication.run(MockedAPIs.class, args).registerShutdownHook();
    }
}
