package com.cimb.vietnammobile.tool.promotion.mocked.api.model.response;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class TopupRestfulResponse {
    private String isdn;
    private String transactionid;
    private String message;
    private Integer resultcode;

    public TopupRestfulResponse() {
    }

    private TopupRestfulResponse(Builder builder) {
        isdn = builder.isdn;
        transactionid = builder.transactionid;
        message = builder.message;
        resultcode = builder.resultcode;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getIsdn() {
        return isdn;
    }

    public String getTransactionid() {
        return transactionid;
    }

    public String getMessage() {
        return message;
    }

    public Integer getResultcode() {
        return resultcode;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.JSON_STYLE)
                .append("isdn", isdn)
                .append("transactionid", transactionid)
                .append("message", message)
                .append("resultcode", resultcode)
                .toString();
    }

    public static final class Builder {
        private String isdn;
        private String transactionid;
        private String message;
        private Integer resultcode;

        private Builder() {
        }

        public Builder withIsdn(String isdn) {
            this.isdn = isdn;
            return this;
        }

        public Builder withTransactionid(String transactionid) {
            this.transactionid = transactionid;
            return this;
        }

        public Builder withMessage(String message) {
            this.message = message;
            return this;
        }

        public Builder withResultcode(Integer resultcode) {
            this.resultcode = resultcode;
            return this;
        }

        public TopupRestfulResponse build() {
            return new TopupRestfulResponse(this);
        }
    }
}
